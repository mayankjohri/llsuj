# Hyperlink
Hyperlink can be found by html tag `<a>`.  Please refer **chapter 2** for detailed instruction on selecting proper selector.

Usually following actions are performed on the hyperlink while testing.

In this chapter we will be covering all the operations which arr normally perform on the hyperlink such as
- identifing link using most common identifiers
- clicking
- validating the link text
- validating link url
- validating link presence
- link data attributes, etc

## Identifying link using most common identifiers
Please check chapter "Locators ..." for more details.

## Click a link

### Ruby
In the below examples, links are identified based on the link text and then click operation is performed in the same line of code.

```ruby
# from chapter_3/click_a_link_by_text0.rb
@driver.find_element(:link_text, "Sign in").click()
```

### Python
```python

```

## Click Nth link with exact same label
### ruby


## Click Nth link by CSS
## Verify a link present or not?
## Getting link data attributes
## Test links open a new browser window
