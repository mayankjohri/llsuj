
## Q. What is ???SQL injection????
Ans. SQL Injection is one of the common attacking techniques used by hackers to get the critical data.

Hackers check for any loop hole in the system through which they can pass SQL queries which by passed the security checks and return back the critical data. This is known as SQL injection. It can allow hackers to steal the critical data or even crash a system.

SQL injections are very critical and needs to be avoided. Periodic security testing can prevent these kind of attacks. SQL database security needs to be define correctly and input boxes and special characters should be handled properly.

## Q. List the attributes of Security Testing? 
Ans. There are following seven attributes of Security Testing:

Authentication
Authorization
Confidentiality
Availability
Integrity
Non-repudiation
Resilience