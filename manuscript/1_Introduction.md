# Introduction
-------------

## Author's babbling
------

Welcome to my humble try to document most of the knowledge which I have gained on selenium in various projects.

I also have added solutions to most of the common issues faced by
Selenium developers. I have tried to aggregate them from various sources
including issues which I have faced.

I hope this book(s) helps you in your quest.

## Selenium

Selenium provide one of the best open source solution for UI automation
for web based applications.


### History

Selenium was originally developed by Jason Huggins in 2004 as an
internal tool at `ThoughtWorks`. Huggins was later joined by other
programmers and testers at `ThoughtWorks`, before `Paul Hammant` joined the
team and steered the development of the second mode of operation that
would later become "Selenium Remote Control" (RC). The tool was open
sourced that year.

Later after Huggins joined Google, Selenium webdriver was included.

We will be covering only Selenium webdriver in this book.

## Selenium language bindings

Selenium provide support for many languages either through native or third party solutions. Prominent languages supported are Java, Python, Ruby, C#, JavaScript, PHP etc.

We in our book try to cover Java, Python and Ruby. 


## Install Selenium For
Before we can use selenium, we need to 
### Python
Use `pip` to install the selenium-webdriver.
```bash
pip install selenium --user 
```
### Ruby
If ruby is already installed then use the following command to install the ruby webdriver.
> If you have root access.
```bash
$ gem install selenium-webdriver
```
> If you are regular user
```bash
$ gem install --user-installl selenium-webdriver

>Fetching: rubyzip-1.2.0.gem (100%)
Successfully installed rubyzip-1.2.0
Fetching: childprocess-0.5.9.gem (100%)
Successfully installed childprocess-0.5.9
Fetching: websocket-1.2.3.gem (100%)
Successfully installed websocket-1.2.3
Fetching: selenium-webdriver-2.53.4.gem (100%)
Successfully installed selenium-webdriver-2.53.4
Parsing documentation for rubyzip-1.2.0
Installing ri documentation for rubyzip-1.2.0
Parsing documentation for childprocess-0.5.9
Installing ri documentation for childprocess-0.5.9
Parsing documentation for websocket-1.2.3
Installing ri documentation for websocket-1.2.3
Parsing documentation for selenium-webdriver-2.53.4
Installing ri documentation for selenium-webdriver-2.53.4
Done installing documentation for rubyzip, childprocess, websocket, selenium-webdriver after 5 seconds
4 gems installed
```

### Java
#### Using Maven

### Cross browser testing
### unittest - Python Unit Testing Framework
### TestNG - Java Unit Testing Framework
### <> - Ruby Unit Testing Framework
### Run recipe scripts
#### Python
#### Ruby
> **Note**:
> If currect drivers are not provided then following error message is observed. 
```text
/home/mayank/.gem/ruby/2.2.0/gems/selenium-webdriver-3.0.0.beta3.1/lib/selenium/webdriver/firefox.rb:58:in `driver_path':         Unable to find Mozilla geckodriver. Please download the server from        https://github.com/mozilla/geckodriver/releases and place it        somewhere on your PATH. More info at https://developer.mozilla.org/en-US/docs/Mozilla/QA/Marionette/WebDriver. (Selenium::WebDriver::Error::WebDriverError)
>        from /home/mayank/.gem/ruby/2.2.0/gems/selenium-webdriver-3.0.0.beta3.1/lib/selenium/webdriver/firefox/w3c_bridge.rb:29:in `initialize'
>        from /home/mayank/.gem/ruby/2.2.0/gems/selenium-webdriver-3.0.0.beta3.1/lib/selenium/webdriver/common/driver.rb:49:in `new'
>        from /home/mayank/.gem/ruby/2.2.0/gems/selenium-webdriver-3.0.0.beta3.1/lib/selenium/webdriver/common/driver.rb:49:in `for'
>        from /home/mayank/.gem/ruby/2.2.0/gems/selenium-webdriver-3.0.0.beta3.1/lib/selenium/webdriver.rb:82:in `for'
```

There are two ways to resolve the issue.
- By Adding the driver binary in env variable PATH
- By manually 


#### Java


### Basic Structure
We will be covering only the latest & relevent features of Selenium i.e. we will not be discussing the Selenium RC will not be discussed in these documents. 

#### Web drivers
Web driver is a w3c standard for


## References
- https://en.wikipedia.org/wiki/Selenium_(software)
