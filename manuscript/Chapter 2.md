# Locating web elements
- - -
```text
    2.1 Find element by ID
    2.2 Find element by Name
    2.3 Find element by Link Text
    2.4 Find element by Partial Link Text
    2.5 Find element by XPath
    2.6 Find element by Tag Name
    2.7 Find element by Class Name
    2.8 Find element by CSS Selector
    2.9 Chain find_element to find child elements
    2.10 Find multiple elements
```
One of the most important task in selenium automation is finding the locators for the elements on the page which can be reliably used every time and should not have to be updated very often due to poor selection of locators.

> **Locators**: String using which selenium can reliably identify any element on the page

Selenium provides multiple locator methods, which can help one in finding the reliable locators.

Lot of discussion has already happened on various forums regarding what is the right approach in selecting the locators. In this section, I will present basic key-points which needs to be taken in consideration while finalizing the locators.

Selenium webdriver provides few methods which can be used by define the elements present on the page. Since their name change with language implementation thus check the one which is listed along with your language.
I usually prefer the following process in determining the locators.
I start with ID, Name, Class Name, CSS, Tag Name, Link Text, Partial Link Text and last XPath.
> **Please do not use XPath ever**,
> As they are notorious for changing during the project life cycle

### Java
In Java following are the functions which can be used to identify various identifier types.

```java

```

### Ruby

```ruby
FINDERS =
{
  :class             => 'ClassName',
  :class_name        => 'ClassName',
  :css               => 'CssSelector',
  :id                => 'Id',
  :link              => 'LinkText',
  :link_text         => 'LinkText',
  :name              => 'Name',
  :partial_link_text => 'PartialLinkText',
  :tag_name          => 'TagName',
  :xpath             => 'Xpath',
}

```

Below is the sample html code for which we will try to create a locator
```html
<p class="Description">
        bluh-bluh-bluh. <em id="developer">Developer</em> bluh bluh
</p>
<div>

<div name="username" tag="usertag">Mayank Johri</div>
<a name="blog" href="https://mayankjohri.wordpress.com">My Blog</a>
</div>
```

| Locator           | Example              |
|-------------------|----------------------|
| ID                | find_element(:id, "developer") |
| Name              | find_element(:name, "blog") |
| Link Text         | find_element(:link_text, "My Blog") |
| Partial Link Text | find_element(:partial_link_text, "Blog") |
| XPath             | find_element(:xpath, "//div/div") |
| Tag Name          | find_element(:tag_name, "usertag") |
| Class Name        | find_element(:class, "Description") |
| CSS               | find_element(:css, "#developer") |

### Python


| Locator           | Example                                    |
|-------------------|--------------------------------------------|
| ID                | find_element_by_id("lga")                  |
| Name              | find_element_by_name("blog")               |
| Link Text         | find_element_by_link_text("My Blog")       |
| Partial Link Text | find_element_by_partial_link_text("Blog")  |
| XPath             | find_element_by_xpath("//div/div")         |
| Tag Name          | find_element_by_tag_name("Usertag")        |
| Class Name        | find_element_by_class_name("className")    |
| CSS               | find_element_by_css_selector('#hplogo')    |


---
## Tricks in finding Elements using CSS

### Using Firebug and FirePath
The most common trick of finding the identifiers is by using firefox and its plugins, namely
"firebug" and firepath. Install these two and press F12 to open the developer tools.

It should look something similar to as shown in below image.

![images/firebug_firepath_1.png](images/firebug_firepath_1.png)

### Using Attribute Selectors
You are not restricted to the two special attributes, class and id. You can specify other attributes by using square brackets. Inside the brackets you put the attribute name, optionally followed by a matching operator and a value. Additionally, matching can be made case-insensitive by appending an " i" after the value, but not many browsers support this feature yet. Examples:

[disabled]
    Selects all elements with a "disabled" attribute.
[type='button']
    Selects elements with a "button" type.
[class~=key]
    Selects elements with the class "key" (but not e.g. "keyed", "monkey", "buckeye"). Functionally equivalent to .key.
[lang|=es]
    Selects elements specified as Spanish. This includes "es" and "es-MX" but not "eu-ES" (which is Basque).
[title*="example" i]
    Selects elements whose title contains "example", ignoring case. In browsers that don't support the "i" flag, this selector probably won't match any element.
a[href^="https://"]
    Selects secure links.
img[src$=".png"]
    Indirectly selects PNG images; any images that are PNGs but whose URL doesn't end in ".png" (such as when there's a query string) won't be selected.


### Selectors based on relationships

|Selector |Selects|
|---------|-------|
|A E      |Any E element that is a descendant of an A element (that is: a child, or a child of a child, etc.)|
|A > E    |Any E element that is a child (i.e. direct descendant) of an A element|
|E:first-child |Any E element that is the first child of its parent|
|B + E    |Any E element that is the next sibling of a B element (that is: the next child of the same parent)

#### Child combinator

![images/child-combinator-selector-example.png](images/child-combinator-selector-example.png)
#### Adjacent sibling combinator
![adjacent-selector-example.png](images/adjacent-selector-example.png)

#### General sibling combinator
![general-sibling-example.png](images/general-sibling-example.png)


---
### Cheat codes

|Selector |Example |Example description |CSS|
|---------|--------|--------------------|----|
| .class  | .intro | Selects all elements with class="intro" |1|
| #id     | #firstname | Selects the element with id="firstname" |1|
| *       | *      | Selects all elements |2|
| element | p | Selects all `<p>` elements |1|
| element,element | div, p | Selects all `<div>` elements and all `<p>` elements |1|
| element element |div p |Selects all `<p>` elements inside `<div>` elements |1|
| element>element |div >` p |Selects all `<p>` elements where the parent is a `<div>` element |2|
| element+element |div + p |Selects all `<p>` elements that are placed immediately after `<div>` elements |2|
| element1~element2 | p ~ ul |Selects every `<ul>` element that are preceded by a `<p>` element |3|
| `[attribute]` | `[target]` |Selects all elements with a target attribute |2|
| `[attribute=value]` | `[target=_blank]` |Selects all elements with target="_blank" |2|
| `[attribute~=value]` |`[title~=flower]` |Selects all elements with a title attribute containing the word "flower" |2|
| `[attribute^=value]` | `a[href^="https"]` |Selects every `<a>` element whose href attribute value begins with "https" |3|
| `[attribute$=value]` | `a[href$=".pdf"]` |Selects every `<a>` element whose href attribute value ends with ".pdf" |3|
| `[attribute*=value]` | `a[href*="w3schools"]` |Selects every `<a>` element whose href attribute value contains the substring "w3schools" |3|
|:active |a:active |Selects the active link |1|
|::after |p::after |Insert something after the content of each `<p>` element |2|
|::before |p::before |Insert something before the content of each `<p>` element |2|
|:checked |input:checked |Selects every checked `<input>` element |3|
|:disabled |input:disabled |Selects every disabled `<input>` element |3|
|:empty |p:empty |Selects every `<p>` element that has no childr(including text nodes) |3|
|:enabled |input:enabled |Selects every enabled `<input>` element |3|
|:first-child |p:first-child |Selects every `<p>` element that is the first child of its parent |2|
|::first-letter |p::first-letter |Selects the first letter of every `<p>` element |1|
|::first-line |p::first-line |Selects the first line of every `<p>` element |1|
|:first-of-type |p:first-of-type |Selects every `<p>` element that is the first `<p>` element of its parent |3|
|:focus |input:focus |Selects the input element which has focus |2|
|:hover |a:hover |Selects links on mouse over |1|
|:in-range |input:in-range |Selects input elements with a value within a specified range |3|
|:invalid |input:invalid |Selects all input elements with an invalid value |3|
|:lang(language) |p:lang(it) |Selects every `<p>` element with a lang attribute equal to "it" (Italian) |2|
|:last-child |p:last-child |Selects every `<p>` element that is the last child of its parent |3|
|:last-of-type |p:last-of-type |Selects every `<p>` element that is the last `<p>` element of its parent |3|
|:link |a:link |Selects all unvisited links |1|
|:not(selector) |:not(p) |Selects every element that is not a `<p>` element |3|
|:nth-child(n) |p:nth-child(2) |Selects every `<p>` element that is the second child of its parent |3|
|:nth-last-child(n) |p:nth-last-child(2) |Selects every `<p>` element that is the second child of its parent, counting from the last child |3|
|:nth-last-of-type(n) |p:nth-last-of-type(2) |Selects every `<p>` element that is the second `<p>` element of its parent, counting from the last child |3|
|:nth-of-type(n) |p:nth-of-type(2) |Selects every `<p>` element that is the second `<p>` element of its parent |3|
|:only-of-type |p:only-of-type |Selects every `<p>` element that is the only `<p>` element of its parent |3|
|:only-child |p:only-child |Selects every `<p>` element that is the only child of its parent |3|
|:optional |input:optional |Selects input elements with no "required" attribute |3|
|:out-of-range |input:out-of-range |Selects input elements with a value outside a specified range |3|
|:read-only |input:read-only |Selects input elements with the "readonly" attribute specified |3|
|:read-write |input:read-write |Selects input elements with the "readonly" attribute NOT specified |3|
|:required |input:required |Selects input elements with the "required" attribute specified |3|
|:root |:root |Selects the document's root element |3|
|::selection |::selection |Selects the portion of an element that is selected by a user | |
|:target |#news:target |Selects the current active #news element (clicked on a URL containing that anchor name) |3|
|:valid |input:valid |Selects all input elements with a valid value |3|
|:visited |a:visited |Selects all visited links |1|

---
## Links
* [www.w3.org/TR/css3-selectors/#selectors](www.w3.org/TR/css3-selectors/#selectors)
* [https://developer.mozilla.org/en/docs/Web/Guide/CSS/Getting_started/Selectors](https://developer.mozilla.org/en/docs/Web/Guide/CSS/Getting_started/Selectors)
