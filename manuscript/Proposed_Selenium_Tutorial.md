Selenium WebDriver With Python, Ruby and Java

Setup And Configuration

     
    Python, Ruby & Java Installation - Windows 
     
    Configuration Of Python - Windows
     
    Python Installation And Setup - Mac 
     
    Package Management Using PIP
     
    IDE Options For Python Development
     
    Installing iPython 

Understanding Variables And Data Type

     
    Python Terminal Walkthrough
     
    Understanding Objects And References
     
    Variables Rules
     
    Numbers Data Type And Math Operations
     
    Numbers - Exponentiation And Modulo
     
    Arithmetic Order Of Precedence
     
    Boolean Data Type
     
    Working With Strings
     
    String Methods - Part 1
     
    String Methods - Part 2
     
    More String Slicing And Indexing
     
    Strings Formatting
     
    Strings Quiz *** Test Your Knowledge ***

Advanced Data Types

     
    List And Accessing The Elements
     
    List Methods
     
    List Quiz *** Test Your Knowledge ***
     
    Working With Dictionary 
     
    Nested Dictionary
     
    Dictionary Methods
     
    Dictionary Quiz *** Test Your Knowledge ***
     
    Working With Tuple
     
    Tuple Quiz *** Test Your Knowledge ***

Comparison And Boolean Operators

     
    Working With Comparator
     
    Understanding Boolean Operator
     
    Boolean Operators - Order Of Precedenc

Program Control Flow

     
    Conditional Logic - If Else Condition
     
    While Loop Dem
     
    Break Continue And While/Else
     
    For Loop Dem
     
    Iterating Multiple Lists - Using the Zip Functio
     
    Using Range Function In For Loop

Methods - Working With Reusable Code

     
    Understanding Method
     
    Working With Return Value
     
    Working With Positional / Optional Parameters
     
    Understanding Variable Scope
     
    More Built-In Function
     
    Exercise With Solution *** Homework **

Classes - Object Oriented Programming

     
    Understanding Objects / Classes
     
    Create Your Own Object
     
    Create Your Own Methods
     
    Inheritance
     
    Method Overriding
     
    Exercise With Solution *** Homework ***

Exception Handling

     
    Exception Handling Demo
     
    Finally And Else Block
     
    Exercise With Solution *** Homework ***

Modules

     
    Builtin Modules
     
    Create Your Own Modules

Working With Files

     
    How To Write Data To A File
     
    How To Read A File
     
    File Handling Using "With" And "As" Keywords

Selenium WebDriver -> Setup And Installation

     
    Firebug And FirePath - Important Information
     
    Firebug and Firepath Installatio
     
    Selenium Webdriver Installation - Mac
     
    Selenium Webdriver Installation - Window
     
    Selenium 3.x Update
     
    Selenium WebDriver 3.x Gecko Driver Setup - Mac
     
    Selenium WebDriver 3.x Gecko Driver Setup - Windows

Selenium WebDriver -> Running Tests On Various Browsers

     
    Run Tests On Firefox 
     
    Run Tests On Chrome - Mac 
     
    Run Tests On Chrome - Windows 
     
    Requirements To Run Tests On IE 
     
    Run Tests On IE 
     
    Requirements To Run Tests On Safari 
     
    Run Tests On Safari 
     
    Interview Questions 

Selenium WebDriver -> Finding Elements

     
    Understanding Elements And DOM 
     
    Find Element By Id And Name 
     
    Understanding Dynamic Ids And Selenium Exception
     
    Find Element By Xpath And Css Selectors
     
    Find Element By Link Text 
     
    Find Element By Class Name And Tag Name 
     
    Understanding By Class 
     
    How To Find List Of Elements
     
    Interview Questions

CSS Selectors - Advanced Locators

     
    Using Ids With CSS Selectors To Find Elements 
     
    Using Multiple Css Classes To Find Elements 
     
    Using Wildcards With Css Selectors 
     
    Finding Child Nodes Using Css Selectors
     
    CSS Cheat Sheet

Xpath - Advanced Locators

     
    Difference Between Absolute And Relative Xpath 
     
    How To Build An Effective Xpath
     
    Using Text To Build An Effective Xpath
     
    Build Xpath Using Contains Keyword
     
    Build Xpath Using Starts-with Keyword 
     
    How To Find Parent And Sibling Nodes
     
    Exercise With Solution Interview Question
     
    Xpath Cheat Sheet

Selenium WebDriver -> Working With Web Elements

     
    Browser Interactions Introduction
     
    Browser Interaction - Practical Implementation
     
    How To Click And Type On A Web Element 
     
    How To Find The State Of A Web Element
     
    Radio Buttons And Checkboxes 
     
    Working With Elements List 
     
    Understanding Dropdown Elements 
     
    Working With A Dropdown Element - Practical Example 
     
    How To Work With Hidden Elements
     
    Working With Hidden Elements - Practical Example
     
    Interview Questions 

Practice Exercise

     
    Practice Exercise Question
     
    Practice Exercise Solution

Selenium WebDriver -> Useful Methods And Properties

     
    How To Get The Text On Element
     
    How To Get Value Of Element Attribute 
     
    Generic Method To Find Elements 
     
    How To Check If Element Is Present 
     
    How To Build Dynamic XPath 
     
    Interview Questions

Selenium WebDriver -> Wait Types

     
    Implicit Wait Vs Explicit Wait 
     
    Implicit Wait - Practical Example 
     
    Explicit Wait - Practical Example
     
    Generic Method To Work With Explicit Wait
     
    Interview Questions 

Selenium WebDriver -> Advanced

     
    Calendar Selection Introduction
     
    Calendar Selection - Practical Example
     
    Calendar Selection - Real Time Example 
     
    Autocomplete Introduction 
     
    AutoComplete - Practical Example 
     
    How To Take Screenshots 
     
    Generic Method To Take Screenshots 
     
    Executing Javascript Commands 
     
    How To Find Size Of The Window 
     
    How To Scroll Element Into View 
     
    Interview Questions 

Selenium WebDriver -> Switch Window And IFrames

     
    How To Switch Window Focus
     
    Switch To Window - Practical Example 
     
    How To Work With Iframes 
     
    Switch To IFrame - Practical Example 
     
    Handling Javascript Popup

Selenium WebDriver -> Working With Actions Class

     
    Mouse Hover Actions 
     
    How To Drag And Drop Element On A Web Page 
     
    Working With Sliders Actions

Logging Infrastructure

     
    Introduction To Logging Infrastructure
     
    Changing The Format Of Logs 
     
    Logger - Console Example
     
    Logger - Configuration File Example
     
    How To Write A Generic Custom Logger Utility

Unittest Infrastructure

     
    Unittest Introduction
     
    Writing First Test Case
     
    How To Implement Class Level SetUp And TearDown Methods
     
    How To Assert A Test Method 
     
    How To Run Code From Terminal 
     
    How To Create A Test Suite

Pytest -> Advanced Testing Framework

     
    Pytest Installation And First Script 
     
    Pytest Naming Conventions
     
    How To Work With Pytest Fixtures 
     
    Multiple Ways To Run Test Cases
     
    Conftest - Common Fixtures For Multiple Modules 
     
    How To Maintain Run Order Of Tests
     
    Running Tests Based On Command Line Arguments
     
    Structure Tests In A Test Class
     
    How To Return A Value From Fixtures
     
    Install PyTest HTML Plugin
     
    How To Generate Html Test Report

Automation Framework - Part 1

     
    Automation Framework Introduction
     
    Understanding Framework Structure
     
    Test Scenario Without Framework
     
    Convert Test Case To Page Object Model Framework
     
    Refactor Your Page Object Class - Part 1 
     
    Build Your Custom Selenium Driver Class
     
    Refactor Your Page Object Class - Part 2

Automation Framework - Part 2

     
    Add Logging To Automation Framework
     
    How To Verify Test Case Result 
     
    Complete Login Page Test Cases
     
    Create Conftest To Implement Common Setup Methods
     
    Refactor Conftest - Make Framework More Readable

Automation Framework - Part 3

     
    How To Assert Without Stopping Test Execution
     
    How To Assert Without Stopping Test Execution - Practical Example - 1 
     
    How To Assert Without Stopping Test Execution - Practical Example - 2 
     
    Implement Screenshots In A Framework 
     
    Taking Screenshots On Test Failure 
     
    BasePage And Util Concept Introduction
     
    Inheriting BasePage Class 

Automation Framework -> Practice Exercise

     
    CustomDriver - Additional Methods Before Exercise 
     
    Practice Exercise Question 
     
    Find Element Locators - Solution Part 1 
     
    Create Register Courses Page Class - Solution Part 2 
     
    Create Register Courses Test Class - Solution Part 3 

Data Driven Testing

     
    Setup And Configuration 
     
    Data Driven Testing - Practical Implementation 
     
    Utility To Read CSV Data 
     
    Multiple Data Sets Test Case - Practical Example

Running Complete Test Suite

     
    How To Manage Navigation In Framework
     
    Refactor Login Tests 
     
    How To Run A Test Suite 
     
    Running Test Suite On Chrome 

Conclusion

     
    BONUS: What's Next and other cool free stuff

