Preface
Who should read this book
How to read this book
Recipe test scripts
Send me feedback
1. Introduction
    1.1 Selenium
    1.2 Selenium language bindings
    1.3 Install Selenium For
        1.3.1 Python
        1.3.2 Ruby
        1.3.3 Java
    1.4 Cross browser testing
    1.5 unittest - Python Unit Testing Framework
    1.6 Run recipe scripts

2. Locating web elements
    2.1 Start browser
    2.2 Find element by ID
    2.3 Find element by Name
    2.4 Find element by Link Text
    2.5 Find element by Partial Link Text
    2.6 Find element by XPath
    2.7 Find element by Tag Name
    2.8 Find element by Class Name
    2.9 Find element by CSS Selector
    2.10 Chain find_element to find child elements
    2.11 Find multiple elements
3. Hyperlink
    3.1 Start browser
    3.2 Click a link by text
    3.3 Click a link by ID
    3.4 Click a link by partial text
    3.5 Click a link by XPath
    3.6 Click Nth link with exact same label
    3.7 Click Nth link by CSS
    3.8 Verify a link present or not?
    3.9 Getting link data attributes
    3.10 Test links open a new browser window
4. Button
    4.1 Click a button by text
    4.2 Click a form button by text
    4.3 Submit a form
    4.4 Click a button by ID
    4.5 Click a button by name
    4.6 Click a image button
    4.7 Click a button via JavaScript
    4.8 Assert a button present
    4.9 Assert a button enabled or disabled?
5. TextField and TextArea
    5.1 Enter text into a text field by name
    5.2 Enter text into a text field by ID
    5.3 Enter text into a password field
    5.4 Clear a text field
    5.5 Enter text into a multi-line text area
    5.6 Assert value
    5.7 Focus on a control
    5.8 Set a value to a read-only or disabled text field
    5.9 Set and assert the value of a hidden field
6. Radio button
    6.1 Select a radio button
    6.2 Clear radio option selection
    6.3 Assert a radio option is selected
    6.4 Iterate radio buttons in a radio group
    6.5 Click Nth radio button in a group
    6.6 Click radio button by the following label
    6.7 Customized Radio buttons - iCheck
7. CheckBox
    7.1 Select by name
    7.2 Uncheck a checkbox
    7.3 Assert a checkbox is checked (or not)
    7.4 Customized Checkboxes - iCheck
8. Select List
    8.1 Select an option by text
    8.2 Select an option by value
    8.3 Select an option by iterating all options
    8.4 Select multiple options
    8.5 Clear one selection
    8.6 Clear selection
    8.7 Assert selected option
    8.8 Assert the value of a select list
    8.9 Assert multiple selections
9. Navigation and Browser
    9.1 Go to a URL
    9.2 Visit pages within a site
    9.3 Perform actions from right mouse click context menu such as ‘Back’, ‘Forward’ or ‘Refresh’
    9.4 Open browser in certain size
    9.5 Maximize browser window
    9.6 Move browser window
    9.7 Minimize browser window
    9.8 Scroll focus to control
    9.9 Switch between browser windows or tabs
    9.10 Remember current web page URL, then come back to it later
10. Assertion
    10.1 Assert page title
    10.2 Assert Page Text
    10.3 Assert Page Source
    10.4 Assert Label Text
    10.5 Assert Span text
    10.6 Assert Div text or HTML
    10.7 Assert Table text
    10.8 Assert text in a table cell
    10.9 Assert text in a table row
    10.10 Assert image present
11. Frames
    11.1 Testing Frames
    11.2 Testing IFrame
    11.3 Test multiple iframes
12. Testing AJAX
    12.1 Wait within a time frame
    12.2 Explicit Waits until Time out
    12.3 Implicit Waits until Time out
    12.4 Create your own polling check function
    12.5 Wait AJAX Call to complete using JQuery
13. File Upload and Popup dialogs
    13.1 File upload
    13.2 JavaScript pop ups
    13.3 Modal style dialogs
    13.4 Bypass basic authentication by embedding username and password in URL
    13.5 Internet Explorer modal dialog
    13.6 Popup Handler Approach
    13.7 Handle JavaScript dialog with Popup Handler
    13.8 Basic or Proxy Authentication Dialog
14. Debugging Test Scripts
    14.1 Print text for debugging
    14.2 Write page source or element HTML into a file
    14.3 Take screenshot
    14.4 Leave browser open after test finishes
    14.5 Debug test execution using Debugger
15. Test Data
    15.1 Get date dynamically
    15.2 Get a random boolean value
    15.3 Generate a number within a range
    15.4 Get a random character
    15.5 Get a random string at fixed length
    15.6 Get a random string in a collection
    15.7 Generate random person names, emails, addresses with Faker
    15.8 Generate a test file at fixed sizes
    15.9 Retrieve data from Database
16. Browser Profile and Capabilities
    16.1 Get browser type and version
    16.2 Set HTTP Proxy for Browser
    16.3 Verify file download in Chrome
    16.4 Test downloading PDF in Firefox
    16.5 Bypass basic authentication with Firefox AutoAuth plugin
    16.6 Manage Cookies
    16.7 Headless browser testing with PhantomJS
    16.8 Test responsive websites
    16.9 Set page load timeout
17. Advanced User Interactions
    17.1 Double click a control
    17.2 Move mouse to a control - Mouse Over
    17.3 Click and hold - select multiple items
    17.4 Context Click - right click a control
    17.5 Drag and drop
    17.6 Drag slider
    17.7 Send key sequences - Select All and Delete
    17.8 Click a specific part of an image
18. HTML 5 and Dynamic Web Sites
    18.1 HTML5 Email type field
    18.2 HTML5 Time Field
    18.3 Invoke ‘onclick’ JavaScript event
    18.4 Invoke JavaScript events such as ‘onchange’
    18.5 Scroll to the bottom of a page
    18.6 Select2 - Single Select
    18.7 Select2 - Multiple Select
    18.8 AngularJS web pages
    18.9 Ember JS web pages
    18.10 “Share Location” with Firefox
    18.11 Faking Geolocation with JavaScript
19. Leverage Programming
    19.1 Raise exceptions to fail test
    19.2 Ignorable test statement error
    19.3 Read external file
    19.4 Data-Driven Tests with Excel
    19.5 Data-Driven Tests with CSV
    19.6 Identify element IDs with dynamically generated long prefixes
    19.7 Sending special keys such as Enter to an element or browser
    19.8 Use of unicode in test scripts
    19.9 Extract a group of dynamic data : verify search results in order
    19.10 Verify uniqueness of a set of data
    19.11 Extract dynamic visible data rows from a results table
    19.12 Extract dynamic text following a pattern using Regex
    19.13 Quick extract pattern text in comments with Regex
20. Optimization
    20.1 Assert text in page_source is faster than the text
    20.2 Getting text from more specific element is faster
    20.3 Avoid programming if-else block code if possible
    20.4 Use variable to cache not-changed data
    20.5 Enter large text into a text box
    20.6 Use Environment Variables to change test behaviours dynamically
    20.7 Test web site in two languages
    20.8 Multi-language testing with lookups
21. Gotchas - from 
    21.1 Test starts browser but no execution with blank screen
    21.2 Failed to assert copied text in browser
    21.3 The same test works for Chrome, but not for IE
    21.4 “unexpected tag name ‘input’”
    21.5 Element is not clickable or not visible
22. Selenium Remote Control Server
    22.1 Selenium Server Installation
    22.2 Execute tests in specified browser on another machine
    22.3 Selenium Grid
23. Test Frameworks
    23.1 Types of Framewords
    23.2. Samples 
24. Appendix
    A. Afterword
    B. WYSIWYG HTML editors
            - TinyMCE
            - CKEditor
            - SummerNote
            - CodeMirror
    C. Resources
        - Books
        - Web Sites
        - Tools





------------------
Lesson 1 – Review: advanced, flexible xpaths; main loops and condition statements; sort of clean coding
Why data verification is the main goal
Types of data collections (String, Array, Array of Arrays, Hashes)
Extra lesson – Advance xpath
Lesson 2 – Condition loops (unless; while; try)
Introduction (Why do we need them)
Unless (boundary condition)
While loops
Try
Exceptions vs Asserts
Lesson 3 – Advanced interface manipulations Part 1
Introduction (pre-build Selenium methods)
Checkboxes (marking / unmarking / verification)
Drop-downs (classic selects / written with extended libraries)
Classic inputs and vocab inputs
Lesson 4 – File uploads, drag and drop
File Uploads
Alternative html tags
JavaScript object properties change
Drag and drop
Lesson 5 – SVG
Locator syntax
Locator dependences
Lesson 6 – File imports
Why do we need such thing?
Upload external libs
Read / write properties
Lesson 7 – Email verification
Possible and impossible scenarios
Frame window (iframe)
Lesson 8 – Solving work issues
---- 
https://seleniumhq.github.io/docs/

Best Practices

    Page Object Models
    Domain Specific Language
    Generating Application State
    Mock External Services
    Improved Reporting
    Avoid Sharing State
    Test Independency
    Consider Using a Fluent API
    Fresh Browser Per Test 

Worst Practices

    Captchas
    File Downloads
    HTTP Response Codes
    Gmail, Email, and Facebook Logins
    Test Dependency
    Performance Testing
    Link Spidering 
