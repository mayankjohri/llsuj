/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.waits;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author mayank
 */
@org.junit.runner.RunWith(org.junit.runners.Suite.class)
@org.junit.runners.Suite.SuiteClasses({ExplicitWait_attributeToBeNotEmptyTest.class, ExplicitWait_andOperatorTest.class, ExplicitWait_elementToBeClickableTest.class, ExplicitWait_attributeToBeTest.class, ExplicitWait_AlertTest.class, ExplicitWait_elementSelectionStateToBeTest.class, ExplicitWait_elementToBeSelectedTest.class, ExplicitWait_attributeContainsTest.class, ExplicitWait_orOperatorTest.class, ExplicitWaitTest.class, ExplicitWait_FluentWaitTest.class, ImplicitWaitTest.class, ExplicitWait_notOperatorTest.class, ExplicitWait_title_isTest.class})
public class WaitsSuite {

    @org.junit.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.junit.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.junit.Before
    public void setUp() throws Exception {
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }
    
}
