/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction;

import in.mj.element.interaction.TabsAndWindows.TabsAndWindowsSuite;
import in.mj.element.interaction.actionchains.ActionchainsSuite;
import in.mj.element.interaction.checkbox.CheckboxSuite;
import in.mj.element.interaction.cookies.CookiesSuite;
import in.mj.element.interaction.progressbar.ProgressbarSuite;
import in.mj.element.interaction.radiobutton.RadiobuttonSuite;
import in.mj.element.interaction.select.SelectSuite;
import in.mj.element.interaction.waits.WaitsSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author mayank
 */
@org.junit.runner.RunWith(org.junit.runners.Suite.class)
@org.junit.runners.Suite.SuiteClasses({ElementClickVisibleTest.class, CookiesSuite.class, ElementSendKeyTest.class, CheckboxSuite.class, TabsAndWindowsSuite.class, RadiobuttonSuite.class, SelectSuite.class, ElementClearTest.class, ActionchainsSuite.class, WaitsSuite.class, ProgressbarSuite.class, ElementClickTest.class, ElementClear_FixedTest.class})
public class InteractionSuite {

    @org.junit.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.junit.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.junit.Before
    public void setUp() throws Exception {
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }
    
}
