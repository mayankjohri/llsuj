/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.actionchains;

/**
 *
 * @author mayank
 */
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class Right_Click {

public static void main(String args[]) throws Exception{

       // Initialize WebDriver
       WebDriver driver = new FirefoxDriver();
       // Wait For Page To Load
       driver.manage().timeouts().implicitlyWait(120,TimeUnit.SECONDS);
   
       // Go to Myntra Page 
        driver.get("http://www.myntra.com");   
       // Maximize Window
       driver.manage().window().maximize();
      
      WebElement R1 = driver.findElement(By.cssSelector("#desktop-header-cnt "
              + "> div.desktop-preHeaderContent.desktop-showBanner "
              + "> a:nth-child(2)"));
      
      // Initialize Actions class object
      Actions builder = new Actions(driver);
      
      // Perform Right Click on  MEN and  Open "Men" content in a new tab 
      builder.contextClick(R1).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
      //ContextClick() is a method to use right click 
    
      /* Perform Right Click on  MEN and  Open "Men" content in a new different Window
      
       builder.contextClick(hindiLanguage).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
  
      //closing current driver window 
     */	
      driver.manage().wait(2000);
      driver.close();
		
	}

}
