/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.waits;


import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author mayank
 */
public class ExplicitWait_title_is {

    private static final Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private static final String URL = "http://127.0.0.1:5000/alert";

    public void getData() {
        WebDriver wd = null;
        try {
            System.setProperty("webdriver.opera.driver",
                    "/home/mayank/apps/web_drivers/operadriver");
            wd = new OperaDriver();
            WebDriverWait wait = new WebDriverWait(wd, 2);
            wd.navigate().to(URL);
            LOG.setLevel(Level.INFO);
            boolean b = wait.until(ExpectedConditions.titleIs("0"));
            LOG.log(Level.INFO, "label value: {0}", b);
        } catch (Exception ex) {
            LOG.log(Level.INFO, "Error: {0}", ex);
        } finally {
            if (wd != null) {
                wd.close();
                wd.quit();
            }
        }
    }

    public static void main(String[] args) {
        ExplicitWait_title_is egt = new ExplicitWait_title_is();
        egt.getData();
    }
}
