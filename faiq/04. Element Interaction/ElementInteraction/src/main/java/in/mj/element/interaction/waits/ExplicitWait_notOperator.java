/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.waits;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author mayank
 */
public class ExplicitWait_notOperator {

    private static final Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private static final String URL = "http://localhost:5000/bulma";

    public void getData() {
        WebDriver wd = null;
        try {

            wd = new ChromeDriver();
            WebDriverWait wait = new WebDriverWait(wd, 15);
            wd.manage().window().maximize();
            wd.navigate().to(URL);
            LOG.setLevel(Level.INFO);

            wait.until(
                    ExpectedConditions.not(
                            ExpectedConditions.elementSelectionStateToBe(
                                    By.cssSelector("[type='checkbox']"),
                                    false)));
        } catch (Exception ex) {
            LOG.log(Level.INFO, "Error: {}", ex);
        } finally {
            if (wd != null) {
                wd.close();
                wd.quit();
            }
        }
    }

    public static void main(String[] args) {
        ExplicitWait_notOperator egt = new ExplicitWait_notOperator();
        egt.getData();
    }
}
