/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.tabs;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author mayank
 */
public class NewTab3 {

    public static void main(String[] args) throws InterruptedException {
        boolean px = false;
        String URL = "http://localhost:5000/popups";
        DesiredCapabilities cap = new DesiredCapabilities();

        System.setProperty("webdriver.gecko.driver",
                "/home/mayank/apps/web_drivers/geckodriver");
        if (px == true) {
            Proxy p = new Proxy();
            p.setHttpProxy("proxy:8080");
            cap.setCapability(CapabilityType.PROXY, p);
        }
        WebDriver driver = new FirefoxDriver(cap);
        driver.get(URL);
//        WebElement we = driver.findElement();

        //identify the link in the page, that you want to open in a new tab of your browser's instance
        WebElement link = driver.findElement(By.id("lnkNewWindow"));

//this action will click the WebElement with the link identified above, hold CONTROL Key (or COMMAND if you're using Mac), click the WebElement and relese the Keys.CONTROL (use Keys.COMMAND if you're using Mac).
        new Actions(driver)
                .keyDown(Keys.CONTROL)
                .click(link)
                .keyUp(Keys.CONTROL)
                .build()
                .perform();
        Thread.sleep(3000);
        driver.quit();
    }

}
