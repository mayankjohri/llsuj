/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// NOT WORKING
package in.mj.element.interaction.tabs;

import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author mayank
 */
public class NewTab {

    public static void main(String[] args) throws InterruptedException {
        boolean px = false;
        String URL = "http://localhost:5000/popups";
        DesiredCapabilities cap = new DesiredCapabilities();

        System.setProperty("webdriver.gecko.driver",
                "/home/mayank/apps/web_drivers/geckodriver");
        if (px == true) {
            Proxy p = new Proxy();
            p.setHttpProxy("proxy:8080");
            cap.setCapability(CapabilityType.PROXY, p);
        }
        WebDriver driver = new FirefoxDriver(cap);
        driver.get(URL);
        WebElement we = driver.findElement(By.id("lnkNewWindow"));
        we.click();
        ArrayList<String> tabs2 = new ArrayList<>(driver.getWindowHandles());
        System.out.println(tabs2.size());
        driver.switchTo().window(tabs2.get(1));
        driver.close();
        driver.switchTo().window(tabs2.get(0));

        Thread.sleep(2000);
        driver.quit();
    }

}
