/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.progressbar;

/**
 *
 * @author mayank
 */
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class CheckboxHtml5ProgressBar {

    private static final Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public void getData() {
        WebDriver webdriver = null;
        try {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.setBinary("/home/mayank/apps/chrome/chrome");
            webdriver = new ChromeDriver(chromeOptions);
            webdriver.get("http://localhost:5000/html5_progress");
            LOG.setLevel(Level.INFO);
            String selector = "progress";
            WebElement we = webdriver.findElement(By.tagName(selector));
            LOG.log(Level.INFO, "label value: {0}", we.getAttribute("value"));

        } catch (Exception ex) {
            LOG.log(Level.INFO, "label value: {0}", ex.getMessage());
        } finally {
            if (webdriver != null) {
                webdriver.close();
                webdriver.quit();
            }
        }
    }

    public static void main(String[] args) {
        CheckboxHtml5ProgressBar egt = new CheckboxHtml5ProgressBar();
        egt.getData();
    }
}
