/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.cookies;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;

/**
 *
 * @author mayank
 */
public class GetCookies {

    private static final Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private static final String URL = "http://localhost:5000/cookies";

    public void getData() {
        WebDriver webdriver = null;
        try {

            System.setProperty("webdriver.opera.driver",
                    "/home/mayank/apps/web_drivers/operadriver");

            webdriver = new OperaDriver();
            webdriver.navigate().to(URL);
            LOG.setLevel(Level.INFO);
//            String css = ;
            for (Cookie cookie : webdriver.manage().getCookies()) {
                LOG.log(Level.INFO, "Cookies: {0}", cookie.toString());
            }
        } catch (Exception ex) {
            LOG.log(Level.INFO, "Error: {0}", ex.getMessage());
        } finally {
            if (webdriver != null) {
                webdriver.close();
                webdriver.quit();
            }
        }
    }

    public static void main(String[] args) {
        GetCookies egt = new GetCookies();
        egt.getData();
    }
}
