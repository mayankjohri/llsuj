/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.alerts;

import in.mj.element.interaction.utils.Mayautils;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author mayank
 */
public class Confirm_Dismiss {

    private static final Logger LOG = Logger.getLogger(
            Logger.GLOBAL_LOGGER_NAME);

    public void getData() throws InterruptedException {
        WebDriver wd = null;
        try {
            LOG.setLevel(Level.INFO);
            Mayautils mu = new Mayautils();
            wd = mu.select_browser_v1("opera");
            wd.navigate().to("http://127.0.0.1:5000/popups");
            WebElement newPopup = wd.findElement(By.id("btnConfirm"));
            newPopup.click();
            new WebDriverWait(wd, 10).until(
                    ExpectedConditions.alertIsPresent());
            Alert al = wd.switchTo().alert();
            // lets read the text on alert
            LOG.log(Level.INFO, "Alert Message: {0}", al.getText());
            al.dismiss();
        } catch (SecurityException ex) {
            LOG.log(Level.INFO, "Error: {0}", ex.getMessage());
        } finally {
            if (wd != null) {
                Thread.sleep(3000);
                wd.close();
                wd.quit();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Confirm_Dismiss obj = new Confirm_Dismiss();
        obj.getData();
    }
}
