/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.utils;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author mayank
 */
public class Mayautils {

    public WebDriver select_browser_v1(String browser_name) {
        return select_browser_v1(browser_name, false);
    }

    /**
     *
     * @param browser_name
     * @param proxy
     * @return
     */
    public WebDriver select_browser_v1(String browser_name, boolean proxy) {
        WebDriver wd = null;
        DesiredCapabilities cap = new DesiredCapabilities();

        if (proxy == true) {
            Proxy p = new Proxy();
            // please update the /etc/hosts or 
            // %windir%/system32/drivers/etc/hosts)file with 
            // <your_proxy_ip>  proxy
            p.setHttpProxy("proxy:8080");
            cap.setCapability(CapabilityType.PROXY, p);
        }
        switch (browser_name) {
            case "chrome":
                wd = new ChromeDriver(cap);
                break;
            case "ff":
                System.setProperty("webdriver.gecko.driver",
                        "/home/mayank/apps/web_drivers/geckodriver");
                wd = new FirefoxDriver(cap);
                break;
            case "opera":
                System.setProperty("webdriver.opera.driver",
                        "/home/mayank/apps/web_drivers/operadriver");

                wd = new OperaDriver();
                break;
        }
        return wd;
    }
}
