/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction;

/**
 *
 * @author mayank
 */
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ElementClickVisible {

    private static final Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public void getData() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setBinary("/home/mayank/apps/chrome/chrome");
        WebDriver webdriver = new ChromeDriver(chromeOptions);
        webdriver.navigate().to("http://127.0.0.1:5000/");
        WebElement button_log;
        button_log = webdriver.findElement(By.cssSelector("[value='Arjun']"));
        LOG.setLevel(Level.INFO);

        LOG.log(Level.INFO, "label value: {0}", button_log.isSelected());
        button_log.click();
        LOG.log(Level.INFO, "label value: {0}", button_log.isSelected());
        webdriver.close();
        webdriver.quit();
    }

    public static void main(String[] args) {
        ElementClickVisible egt = new ElementClickVisible();
        egt.getData();
    }
}
