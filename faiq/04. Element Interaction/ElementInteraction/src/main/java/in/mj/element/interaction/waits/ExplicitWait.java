/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.waits;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author mayank
 */
public class ExplicitWait {
    private static final Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private static final String URL = "https://127.0.0.1:4040/cookies";

    public void getData() {
        WebDriver wd = null;
        try {

            System.setProperty("webdriver.opera.driver",
                    "/home/mayank/apps/web_drivers/operadriver");
            DesiredCapabilities dc = new DesiredCapabilities();
            dc.setCapability("ACCEPT_SSL_CERTS", true);
            wd = new OperaDriver(dc);
            wd.navigate().to(URL);
            LOG.setLevel(Level.INFO);
            wd.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            // this should through error
            wd.findElement(By.className("not_found"));
        } catch (Exception ex) {
            LOG.log(Level.INFO, "Error: {0}", ex);
        } finally {
            if (wd != null) {
                wd.close();
                wd.quit();
            }
        }
    }

    public static void main(String[] args) {
        ExplicitWait egt = new ExplicitWait();
        egt.getData();
    }
}
