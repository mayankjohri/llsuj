package in.mj.ElementProperties;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mayank
 */
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class GetParentElement {

    private static final Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public void getData() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setBinary("/home/mayank/apps/chrome/chrome");
        chromeOptions.addArguments("--headless");
        WebDriver webdriver = new ChromeDriver(chromeOptions);
        webdriver.navigate().to("http://127.0.0.1:5000/");
        WebElement childElement = webdriver.findElement(By.cssSelector("input[name='vehicle'][value='Bike']"));
        JavascriptExecutor executor = (JavascriptExecutor) webdriver;
        WebElement parentElement = (WebElement) executor.executeScript("return arguments[0].parentNode;", childElement);
        WebElement p2 = childElement.findElement(By.xpath(".."));
        LOG.setLevel(Level.INFO);
        LOG.log(Level.INFO, "Vals: {0}",  parentElement.getTagName());
        LOG.log(Level.INFO, "Vals: {0}", p2.getTagName());
        webdriver.close();
        webdriver.quit();
    }

    public static void main(String[] args) {
        GetParentElement egt = new GetParentElement();
        egt.getData();
    }
}
