/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.in.mj.app.pageobjects;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 *
 * @author mayank
 */
public class LinksPage {

    @FindBy (css = "a[id='menu']")
    public static WebElement menu_link;
    
    @FindBy (css = "a[id='bulma']")
    public static WebElement bulma_link;
    
    
    @FindBy (css = "a[id='top']")
    public static WebElement popups_link;
    
    @FindBy (id = "sendClear")
    public static WebElement SendClear_link;
    
    @FindBy (id = "success")
    public static WebElement success_link;
    
    @FindBy (linkText = "Open a popup window")
    public static WebElement popupWindow_link;
     
}