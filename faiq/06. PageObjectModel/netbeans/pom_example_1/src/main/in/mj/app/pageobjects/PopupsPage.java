/*
 * Copyright (C) 2018 mayank
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package main.in.mj.app.pageobjects;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author mayank
 */
/*
    Not very efficient implementation. :). We will keep it 
    improving as we proceed. In this version we do not have
    any parent base class thus lot of code is redundant in 
    other page files. 
 */
public final class PopupsPage {

    @FindBy(css = "input[value='Show confirm box']")
    private static WebElement show_confirm_box_button;

    @FindBy(id = "btnAlert")
    private static WebElement show_alert_button;

    @FindBy(xpath = "//*[@id='btnPrompt']")
    private static WebElement show_prompt_button;

    @FindBy(id = "lnkNewWindow")
    public static WebElement new_window_link;

    @FindBy(id = "btnNewNamelessWindow")
    private static WebElement OpenNamelessWindow_button;

    // If we do not want Testers to directly access the elements than make them 
    // private, so that QA are only be able to access it through Functions &
    // Methods
    @FindBy(linkText = "Open Named Window")
    private static WebElement open_named_window_button;

    @FindBys({
        @FindBy(id = "result"),
        @FindBy(id = "output")
    })
    private static WebElement output;

    @FindAll({
        @FindBy(xpath = "/body/span"),
        @FindBy(id = "ref")
    })
    public static WebElement reference_text;

    public WebDriver wd;

    public PopupsPage(WebDriver driver) {
        wd = driver;
        goPage();
        PageFactory.initElements(wd, this);
    }

    public void goPage() {
        String url = "http://127.0.0.1:5000/popups";
        wd.navigate().to(url);
    }

    public String getreference_text() {
        return reference_text.getText();
    }

    public String getOutput() {
        return output.getText();
    }

    private void click_show_confirm_box_button() {
        show_confirm_box_button.click();
    }

    public String clickConfirm_show_confirm_box_button() {
        click_show_confirm_box_button();
        Alert al = wd.switchTo().alert();
        String txt = al.getText();
        al.accept();
        return txt;
    }

    public void click_show_alert_button() {
        show_alert_button.click();
    }

    public void click_show_prompt_button() {
        show_prompt_button.click();
    }

    public void click_OpenNamelessWindow_button() {
        OpenNamelessWindow_button.click();
    }
}
