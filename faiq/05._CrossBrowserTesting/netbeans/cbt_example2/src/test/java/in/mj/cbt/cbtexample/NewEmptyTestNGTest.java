/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.java.in.mj.cbt.cbtexample;

import main.in.mj.cbt.cbtexample.framework.utils.MayaUtils;
import static org.testng.Assert.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 *
 * @author mayank
 */
public class NewEmptyTestNGTest {

    protected WebDriver wd;
    protected MayaUtils mu;
    public NewEmptyTestNGTest() {

    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void index_email_label() {
        assertEquals("Email address",
                wd.findElement(
                        By.cssSelector("label[for='exampleInputEmail1']")).getText(),
                "Email text is not same");
    }

    @BeforeClass
    @Parameters ("browser_type")
    public void setUpClass(String browserName) throws Exception {
        mu = new MayaUtils();
        wd = mu.select_browser_v1(browserName);

    }

    @AfterClass
    public void tearDownClass() throws Exception {
        wd.close();
        wd.quit();
        mu = null;
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
        wd.navigate().to("http://127.0.0.1:5000/");
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
}
