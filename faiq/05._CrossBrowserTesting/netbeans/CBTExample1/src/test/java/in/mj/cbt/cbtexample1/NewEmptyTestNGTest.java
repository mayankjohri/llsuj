/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.java.in.mj.cbt.cbtexample1;

//import static org.testng.Assert.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author mayank
 */
public class NewEmptyTestNGTest {
    protected WebDriver wd;
    public NewEmptyTestNGTest() {
        
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void index_email_label(){
        Assert.assertEquals("Email address", 
                wd.findElement(
                        By.cssSelector("label[for='exampleInputEmail1']")).getText(), 
                "Email text is not same");
    }
    
    @BeforeClass
    public void setUpClass() throws Exception {
        wd = new ChromeDriver();
        
    }

    @AfterClass
    public  void tearDownClass() throws Exception {
        wd.close();
        wd.quit();
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
        wd.navigate().to("http://127.0.0.1:5000/");
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
}
