package in.co.mj.selenium_namaskar;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class ElementGetSize {
	private static final Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public void getSize() {
		System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
		FirefoxOptions options = new FirefoxOptions();
		options.setBinary("/home/mayank/apps/firefox/firefox"); // Location where Firefox is installed
		FirefoxOptions firefoxOptions = new FirefoxOptions();

		firefoxOptions.setCapability("moz:firefoxOptions", options);
		firefoxOptions.setHeadless(true);
		FirefoxDriver webdriver = new FirefoxDriver(firefoxOptions);
		webdriver.get("https://www.amazon.in/dp/B07BQHYKM7");
		Dimension val;
		val = webdriver.findElementById("ebooksProductTitle").getSize();
		LOG.setLevel(Level.INFO);
		LOG.log(Level.INFO, "Title {0}", val);
		webdriver.close();
	}

	public static void main(String[] args) {
		ElementGetSize egt = new ElementGetSize();
		egt.getSize();
	}
}
