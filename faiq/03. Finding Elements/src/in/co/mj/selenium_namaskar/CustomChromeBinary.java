package in.co.mj.selenium_namaskar;

import java.util.Arrays;
import java.util.HashMap;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class CustomChromeBinary {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
		ChromeOptions options = new ChromeOptions();
		options.setBinary("/home/mayank/apps/chrome/opt/google/chrome/google-chrome");
	    options.setCapability("chrome.switches",Arrays.asList("--no-default-browser-check")); 
	    HashMap<String, Boolean>chromePreferences = new HashMap<>(); 
	    chromePreferences.put("profile.password_manager_enabled", false);
	    options.setCapability("chrome.prefs", chromePreferences); 
	    ChromeDriver webdriver = new ChromeDriver(options);
	    
	    webdriver.get("https://www.amazon.in/dp/B07BQHYKM7");
	    webdriver.close();
	}
}

