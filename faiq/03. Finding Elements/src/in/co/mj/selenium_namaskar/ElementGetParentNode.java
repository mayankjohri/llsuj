package in.co.mj.selenium_namaskar;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ElementGetParentNode {
	private static final Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public void getData() {
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.setBinary("/home/mayank/apps/chrome/chrome");
		chromeOptions.addArguments("--headless");

		WebDriver webdriver = new ChromeDriver(chromeOptions);

		webdriver.navigate().to("http://127.0.0.1:5000/");
		WebElement childElement = webdriver.findElement(By.cssSelector("input[name='vehicle'][value='Bike']"));
		JavascriptExecutor executor = (JavascriptExecutor) webdriver;
		WebElement parentElement = (WebElement) executor.executeScript("return arguments[0].parentNode;", childElement);
		WebElement p2 = childElement.findElement(By.xpath(".."));
		LOG.setLevel(Level.INFO);

		LOG.log(Level.INFO, "Vals: " + parentElement.getTagName());
		LOG.log(Level.INFO, "Vals: " + p2.getTagName());
		webdriver.close();
	}

	public static void main(String[] args) {
		ElementGetParentNode egt = new ElementGetParentNode();
		egt.getData();
	}
}
