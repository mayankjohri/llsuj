package in.co.mj.selenium_namaskar;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.Rectangle;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class ElementGetRect {
	private static final Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public void getData() {
		System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
		FirefoxOptions options = new FirefoxOptions();
		options.setBinary("/home/mayank/apps/firefox/firefox"); // Location where Firefox is installed
		FirefoxOptions firefoxOptions = new FirefoxOptions();

		firefoxOptions.setCapability("moz:firefoxOptions", options);
		firefoxOptions.setHeadless(true);
		FirefoxDriver webdriver = new FirefoxDriver(firefoxOptions);
		webdriver.get("https://www.amazon.in/dp/B07BQHYKM7");
		Rectangle val;
		val = webdriver.findElementByXPath("//li/b[contains(text(),'Word Wise:')]/parent::*").getRect();
		LOG.setLevel(Level.INFO);

		LOG.log(Level.INFO, "Vals: " + val.getX() + " " 
									 + val.getY() + " "
									 + val.getWidth() + " " 
									 + val.getHeight());
		webdriver.close();
	}

	public static void main(String[] args) {
		ElementGetRect egt = new ElementGetRect();
		egt.getData();
	}
}
