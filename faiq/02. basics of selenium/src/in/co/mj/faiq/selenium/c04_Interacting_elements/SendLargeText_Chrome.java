package in.co.mj.faiq.selenium.c04_Interacting_elements;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class SendLargeText_Chrome {

    public static void setLargeText(ChromeDriver driver, WebElement element, String large_text) {
        driver.executeScript("arguments[0].value=arguments[1]",
                element, large_text);
    }

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        ChromeOptions options = new ChromeOptions();
        options.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
        ChromeDriver driver = new ChromeDriver(options);
        driver.get("http://127.0.0.1:5000/bulma");

        WebElement text_area = driver.findElement(By.cssSelector("textarea[class='textarea']"));
        String large_text = "Holi is celebrated as the conclusion of winter and the start of spring\n"
                + "to sow the land and hope for a good harvest. This day is marked by\n"
                + "colors and song (Chautal). It does not require specific prayer or\n"
                + "fasting, however some people keep a vegetarian fast on this day.\n"
                + "The Arya Samaj does not associate Holi with a particular deity such as\n"
                + "Vishnu or Shiva and in comparison to some interpretations of the\n"
                + "festival, the Arya Samaj version in more sober and is as per the 4 Vedas";
        
        setLargeText(driver, text_area, large_text);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SendLargeText_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.quit();
    }
}
