/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.mj.faiq.selenium.c02_customize_browsers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

/**
 *
 * @author mayank
 */
public class Screenshot_Firefox {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        FirefoxOptions options = new FirefoxOptions();
        options.setHeadless(true);
        options.setBinary("/home/mayank/apps/firefox/firefox"); //Location where Firefox is installed
        FirefoxDriver webdriver = new FirefoxDriver(options);
        webdriver.get("http://localhost:5000/bulma");
        String s = webdriver.findElementById("username").getAttribute("value");
        File source = ((TakesScreenshot) webdriver).getScreenshotAs(OutputType.FILE);
        File dest = new File("screenshot_fullscreen" + ".png");
        try {
            Files.copy(source.toPath(), dest.toPath());
        } catch (IOException ex) {
            Logger.getLogger(Element_ScreenShot_Firefox.class.getName()).log(Level.SEVERE, null, ex);
        }
        webdriver.getCurrentUrl();
        
        webdriver.quit();
    }
}
