package in.co.mj.faiq.selenium.c03_finding_elements;

import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ImageStatus_Firefox {

    public static Boolean ImageStatus(WebDriver driver, By by) {
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;

            String script = String.join(System.getProperty("line.separator"),
                    "return arguments[0].complete ",
                    " && typeof arguments[0].naturalWidth != \"undefined\" ",
                    " &&  arguments[0].naturalWidth > 0"
            );
            Boolean imageLoaded;
            imageLoaded = (Boolean) js.executeScript(script, driver.findElement(by));

            return imageLoaded;

        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        WebDriver driver;
        driver = new FirefoxDriver();
        driver.get("http://127.0.0.1:5000/imgs");

        List<String> imgs = Arrays.asList("a", "b", "c", "d");
        for (String a : imgs) {
            System.out.println(ImageStatus(driver,
                    By.id(a)));
        }
        driver.getTitle();
     

        driver.quit();
    }
}
