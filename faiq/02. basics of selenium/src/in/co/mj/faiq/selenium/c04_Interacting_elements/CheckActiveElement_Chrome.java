package in.co.mj.faiq.selenium.c04_Interacting_elements;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class CheckActiveElement_Chrome {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        ChromeOptions options = new ChromeOptions();
        options.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
        ChromeDriver driver = new ChromeDriver(options);
        driver.get("http://127.0.0.1:5000/bulma");
        
        WebElement username = driver.findElement(By.id("username"));
        WebElement name = driver.findElement(By.id("name"));
        username.click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(CheckActiveElement_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }
        WebElement we = driver.switchTo().activeElement();
        System.out.println(name == we);
        System.out.println(username == we);
        System.out.println(we);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(CheckActiveElement_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.quit();
    }
}
