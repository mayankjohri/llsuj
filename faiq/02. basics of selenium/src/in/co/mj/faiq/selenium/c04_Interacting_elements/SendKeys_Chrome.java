package in.co.mj.faiq.selenium.c04_Interacting_elements;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class SendKeys_Chrome {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        ChromeOptions options = new ChromeOptions();
        options.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
        ChromeDriver driver = new ChromeDriver(options);
        driver.get("http://127.0.0.1:5000/bulma");
        
        WebElement text_area = driver.findElement(By.cssSelector("textarea[class='textarea']"));
        text_area.sendKeys(Keys.CONTROL + "a");
        text_area.sendKeys(Keys.CONTROL + "c");
        text_area.clear();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SendKeys_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }
        text_area.sendKeys(Keys.CONTROL + "v");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SendKeys_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.quit();
    }
}
