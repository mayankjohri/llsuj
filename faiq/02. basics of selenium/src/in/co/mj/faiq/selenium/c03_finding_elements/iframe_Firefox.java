package in.co.mj.faiq.selenium.c03_finding_elements;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class iframe_Firefox {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://127.0.0.1:5000/iframe");
        driver.switchTo().frame("i_popups");
        WebElement element = driver.findElement(By.id("lnkNewWindow"));
        System.out.println("target: " + element.getAttribute("target"));
        driver.switchTo().defaultContent();
        try {
            Thread.sleep(2);
        } catch (InterruptedException ex) {
            Logger.getLogger(iframe_Firefox.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.quit();
    }
}
