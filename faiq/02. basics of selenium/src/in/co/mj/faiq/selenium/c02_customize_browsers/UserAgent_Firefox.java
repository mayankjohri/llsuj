package in.co.mj.faiq.selenium.c02_customize_browsers;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

public class UserAgent_Firefox {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        DesiredCapabilities caps = new DesiredCapabilities();
        FirefoxProfile profile = new FirefoxProfile();
        FirefoxOptions options = new FirefoxOptions();

        profile.setPreference("general.useragent.override", "God-Browser");
        options.setProfile(profile);
        caps.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
        WebDriver driver = new FirefoxDriver(caps);

        try {
            driver.get("http://127.0.0.1:5000/bulma");
            // View the details in network request panel <- F12
            Thread.sleep(30000);
        } catch (InterruptedException ex) {
            Logger.getLogger(UserAgent_Firefox.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            driver.quit();
        }
    }
}
