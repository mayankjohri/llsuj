package in.co.mj.faiq.selenium.c02_customize_browsers;

import java.util.Arrays;
import java.util.logging.Level;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class ReadPERFORMANCELog_Chrome {

    public static void main(String[] args) {

        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        ChromeOptions options = new ChromeOptions();
        options.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
        cap.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        cap.setCapability(ChromeOptions.CAPABILITY, options);
        ChromeDriver driver = new ChromeDriver(cap);

        driver.get("http://127.0.0.1:5000/bulma");
        WebElement element = driver.findElement(By.tagName("textarea"));
        element.click();
        driver.executeScript("console.log(5 + 6);");
        element.clear();
        driver.get("http://127.0.0.1:5000/alert");

        for (LogEntry entry : driver.manage().logs().get(LogType.PERFORMANCE)) {
            System.out.println(entry.toString());
        }

        driver.quit();
    }
}
