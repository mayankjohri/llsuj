package in.co.mj.faiq.selenium.c04_Interacting_elements;

import java.util.Arrays;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

public class ScrollToElement_Chrome {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        ChromeOptions options = new ChromeOptions();
        options.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
        ChromeDriver driver = new ChromeDriver(options);
        driver.get("http://127.0.0.1:5000/bulma");
        
        WebElement submit_button = driver.findElement(By.cssSelector("button[class='button is-link']"));
        
        Actions action = new Actions(driver);
        
        action.moveToElement(submit_button);
        action.perform();
        
        driver.quit();
    }
}
