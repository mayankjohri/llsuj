package in.co.mj.faiq.selenium.c03_finding_elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ReadInlineSVG_Firefox {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://127.0.0.1:5000/timeout");
        WebElement element = driver.findElement(By.cssSelector("rect[id='my_rect']"));
        System.out.println("Weight: " + element.getAttribute("width") 
                         + ", Height: " + element.getAttribute("height"));
        driver.quit();
    }
}