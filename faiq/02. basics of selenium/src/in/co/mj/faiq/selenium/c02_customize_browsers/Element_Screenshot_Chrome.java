package in.co.mj.faiq.selenium.c02_customize_browsers;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Element_Screenshot_Chrome {

    public static void captureElementScreenshot(WebDriver driver,
            WebElement element,
            String file_name) throws IOException {

        File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        int ImageWidth = element.getSize().getWidth();
        int ImageHeight = element.getSize().getHeight();

        Point point = element.getLocation();
        int xcord = point.getX();
        int ycord = point.getY();

        BufferedImage img = ImageIO.read(screen);
        BufferedImage dest = img.getSubimage(xcord, ycord, ImageWidth, ImageHeight);
        ImageIO.write(dest, "png", screen);

        File dest_file = new File(file_name);
        Files.copy(screen.toPath(), dest_file.toPath());
    }

    public static void main(String[] args) {

        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        ChromeOptions options = new ChromeOptions();
        options.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
        ChromeDriver driver = new ChromeDriver(options);

        driver.get("http://127.0.0.1:5000/bulma");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Element_Screenshot_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }

        WebElement element = driver.findElement(By.tagName("textarea"));
        try {
            captureElementScreenshot(driver, element, "screenshot_chrome.png");
        } catch (IOException ex) {
            Logger.getLogger(Element_Screenshot_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.quit();
    }
}
