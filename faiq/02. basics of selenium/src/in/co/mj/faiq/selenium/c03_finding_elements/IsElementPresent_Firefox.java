package in.co.mj.faiq.selenium.c03_finding_elements;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class IsElementPresent_Firefox {

    public static WebDriver driver;

    public static boolean isElementPresentUsingElement(WebDriver driver, By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static boolean isElementPresentUsingSize(WebDriver driver, By by) {
        try {
            return driver.findElements(by).size() > 0;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        driver = new FirefoxDriver();
        driver.get("http://127.0.0.1:5000/bulma");
        System.out.println(isElementPresentUsingElement(driver,
                By.id("username")));
        System.out.println(isElementPresentUsingSize(driver,
                By.id("username")));
        driver.quit();
    }
}
