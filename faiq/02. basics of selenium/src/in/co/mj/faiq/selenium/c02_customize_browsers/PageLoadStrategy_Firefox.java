package in.co.mj.faiq.selenium.c02_customize_browsers;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class PageLoadStrategy_Firefox {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("pageLoadStrategy", "none");
        WebDriver driver = new FirefoxDriver(caps);

        try {
            driver.get("https://google.com");
            System.out.println("This should be printed before page is loaded");
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(PageLoadStrategy_Firefox.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            driver.quit();
        }
    }
}
