package in.co.mj.faiq.selenium.c03_finding_elements;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class GetElementAttributes_Firefox {

    public static Map<String, Object>  GetAllAttributes(WebDriver driver, By by) {
        JavascriptExecutor js = (JavascriptExecutor) driver;

        String script = String.join(System.getProperty("line.separator"),
                "var items = {};",
                "for (index = 0; index < arguments[0].attributes.length; ++index) {",
                "items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value };",
                "return items;"
        );
        Map<String, Object>  ls;
        ls = (Map<String, Object>) js.executeScript(script, driver.findElement(by));
        return ls;
    }

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        WebDriver driver;
        driver = new FirefoxDriver();
        driver.get("http://127.0.0.1:5000/bulma");
        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            Logger.getLogger(GetElementAttributes_Firefox.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(GetAllAttributes(driver, By.cssSelector("[type='email']")));
        System.out.println("~^~^~^~^ Done");
        driver.quit();
    }
}
