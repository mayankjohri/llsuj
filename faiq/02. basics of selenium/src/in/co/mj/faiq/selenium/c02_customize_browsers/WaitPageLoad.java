package in.co.mj.faiq.selenium.c02_customize_browsers;

import java.util.Arrays;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class WaitPageLoad {

    public static void main(String[] args) {

        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        ChromeOptions options = new ChromeOptions();
        options.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
        ChromeDriver webdriver = new ChromeDriver(options);

        webdriver.get("http://127.0.0.1:5000/bulma");
        webdriver.close();
    }
}
