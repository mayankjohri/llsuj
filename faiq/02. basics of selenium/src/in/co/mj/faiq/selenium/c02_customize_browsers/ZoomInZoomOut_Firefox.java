package in.co.mj.faiq.selenium.c02_customize_browsers;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ZoomInZoomOut_Firefox {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        WebDriver driver = new FirefoxDriver();

        try {
            driver.get("http://127.0.0.1:5000/");
            Thread.sleep(1000);

            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("document.body.style.transform = 'scale(2)';"
                    + "document.body.style.transformOrigin = '0 0';");
            Thread.sleep(1000);
            js.executeScript("document.body.style.transform = 'scale(1)';"
                    + "document.body.style.transformOrigin = '0 0';");
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ZoomInZoomOut_Firefox.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            driver.quit();
        }
    }
}
