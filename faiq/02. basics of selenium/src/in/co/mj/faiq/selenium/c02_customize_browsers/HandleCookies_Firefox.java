package in.co.mj.faiq.selenium.c02_customize_browsers;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

public class HandleCookies_Firefox {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        DesiredCapabilities caps = new DesiredCapabilities();
        FirefoxProfile profile = new FirefoxProfile();
        FirefoxOptions options = new FirefoxOptions();

//        profile.setPreference("general.useragent.override", "God-Browser");
        options.setProfile(profile);
        caps.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
        WebDriver driver = new FirefoxDriver(caps);

        try {
            driver.get("http://127.0.0.1:5000/bulma");
            Cookie name = new Cookie("Wireless power transfer", "Nikola Tesla");
            driver.manage().addCookie(name);
            name = new Cookie("Value of PI", "Aryabhata");
            driver.manage().addCookie(name);
            name = new Cookie("Bulb", "Thomas Edison ");
            driver.manage().addCookie(name);
            Set<Cookie> cookiesList = driver.manage().getCookies();
            for (Cookie getcookies : cookiesList) {
                System.out.println(getcookies);
            }
            driver.manage().deleteCookieNamed("Bulb");
            cookiesList = driver.manage().getCookies();
            for (Cookie getcookies : cookiesList) {
                System.out.println(getcookies);
            }
        } catch (Exception ex) {
            Logger.getLogger(HandleCookies_Firefox.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            driver.quit();
        }
    }
}
