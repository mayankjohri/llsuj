package in.co.mj.faiq.selenium.c04_Interacting_elements;

import java.util.Arrays;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

public class MultiPopupsClose_Chrome {

    public static void closeAllChilds(ChromeDriver driver, String defaultWindow) {
        Set<String> allWindowHandles = driver.getWindowHandles();

        for (String handle : allWindowHandles) {
            if(! handle.equals(defaultWindow)){
                driver.switchTo().window(handle);
                driver.close();
            }
        }
    }

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        ChromeOptions options = new ChromeOptions();
        options.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
        ChromeDriver driver;
        driver = new ChromeDriver(options);
        driver.get("http://127.0.0.1:5000/welcome_popups");

        WebElement welcome;
        welcome = driver.findElement(By.id("welcome"));
        Actions action;
        action = new Actions(driver);
        action.click(welcome).click(welcome).click(welcome).build().perform();
         try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(MultiPopupsClose_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }
        String parentWindowHandle = driver.getWindowHandle();
        closeAllChilds(driver, parentWindowHandle);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(MultiPopupsClose_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.quit();
    }
}
