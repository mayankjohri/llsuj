package in.co.mj.faiq.selenium.c02_customize_browsers;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Handle_InvalidCert_Chrome {

    public static void main(String[] args) {

        ChromeOptions options = new ChromeOptions();
        options.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
//        options.setAcceptInsecureCerts(true);
//        options.addArguments("--ignore-certificate-errors");

        ChromeDriver driver = new ChromeDriver(options);

        driver.get("https://expired.badssl.com/");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Handle_InvalidCert_Firefox_2.class.getName()).log(Level.SEVERE, null, ex);
        }

        driver.quit();
    }
}
