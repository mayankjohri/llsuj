package in.co.mj.faiq.selenium.c02_customize_browsers;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class SetProxy_Firefox {

    public static void main(String[] args) {

        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        FirefoxOptions options = new FirefoxOptions();

        String PROXY = "localhost:8080";

        org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();
        proxy.setHttpProxy(PROXY)
                .setFtpProxy(PROXY)
                .setSslProxy(PROXY);
        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability(CapabilityType.PROXY, proxy);

        options.setBinary("/home/mayank/apps/firefox/firefox");
        cap.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
        WebDriver driver = new FirefoxDriver(cap);

        driver.get("https://untrusted-root.badssl.com/");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SetProxy_Firefox.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.quit();
    }
}
