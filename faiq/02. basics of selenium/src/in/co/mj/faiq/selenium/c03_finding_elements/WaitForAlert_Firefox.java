package in.co.mj.faiq.selenium.c03_finding_elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitForAlert_Firefox {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://127.0.0.1:5000/alert_now");
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.alertIsPresent());

        String alrt = driver.switchTo().alert().getText();
        System.out.println(alrt);
        driver.switchTo().defaultContent();
        driver.quit();
    }
}
