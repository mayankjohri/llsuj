package in.co.mj.faiq.selenium.c04_Interacting_elements;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class CustomJavaScript_Chrome {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        ChromeOptions options = new ChromeOptions();
        options.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
        ChromeDriver driver = new ChromeDriver(options);
        driver.get("http://127.0.0.1:5000/mwjquery");
        
        String fileName = System.getProperty("user.dir") 
                        + File.separator  
                        + "jquery-git.js";

        String content ="";
        try {
            content = new String(Files.readAllBytes(Paths.get(fileName)));
        } catch (IOException ex) {
            Logger.getLogger(CustomJavaScript_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }

        driver.executeScript(content);
        driver.executeScript("$('.num').text('!!! ~~~ 15 Aug 1947 ~~~ !!!\');");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(CustomJavaScript_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }

        driver.quit();
    }
}
