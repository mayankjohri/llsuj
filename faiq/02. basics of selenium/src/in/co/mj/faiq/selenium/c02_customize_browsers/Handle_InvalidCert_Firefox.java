package in.co.mj.faiq.selenium.c02_customize_browsers;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Handle_InvalidCert_Firefox {

    public static void main(String[] args) {

        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        FirefoxOptions options = new FirefoxOptions();

//        options.addArguments("--ignore-certificate-errors");
        options.setBinary("/home/mayank/apps/firefox/firefox");
        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);

        FirefoxDriver driver = new FirefoxDriver(dc);
        driver.get("https://untrusted-root.badssl.com/");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Handle_InvalidCert_Firefox.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.quit();
    }
}
