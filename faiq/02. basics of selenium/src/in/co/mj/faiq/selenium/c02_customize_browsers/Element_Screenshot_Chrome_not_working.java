package in.co.mj.faiq.selenium.c02_customize_browsers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Element_Screenshot_Chrome_not_working {

    public static void main(String[] args) {

        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        ChromeOptions options = new ChromeOptions();
        options.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
        ChromeDriver driver = new ChromeDriver(options);

        driver.get("http://127.0.0.1:5000/bulma");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Element_Screenshot_Chrome_not_working.class.getName()).log(Level.SEVERE, null, ex);
        }

        WebElement element = driver.findElement(By.tagName("textarea"));
        File source = element.getScreenshotAs(OutputType.FILE);
        File dest = new File("screenshot" + ".png");
        try {
            Files.copy(source.toPath(), dest.toPath());
        } catch (IOException ex) {
            Logger.getLogger(Element_Screenshot_Chrome_not_working.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.quit();
    }
}
