package in.co.mj.faiq.selenium.c03_finding_elements;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class OnlyParentText_Firefox {

    public static String OnlyParentText(WebDriver driver, By by) {
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;

            String script = String.join(System.getProperty("line.separator"),
                    "return jQuery(arguments[0]).contents().filter(function() {",
                    "        return this.nodeType == Node.TEXT_NODE;\n",
                    "    }).text();"
            );
            return (String) js.executeScript(script, driver.findElement(by));

        } catch (NoSuchElementException e) {
            return "!!!~~~ERROR~~~!!!";
        }
    }

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        WebDriver driver;
        driver = new FirefoxDriver();
        driver.get("http://127.0.0.1:5000/nested");
        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            Logger.getLogger(OnlyParentText_Firefox.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(OnlyParentText(driver, By.id("parent")));
        driver.quit();
    }
}