package in.co.mj.faiq.selenium.c04_Interacting_elements;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CheckActiveElement_Firefox {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://127.0.0.1:5000/bulma");
        
        WebElement username = driver.findElement(By.id("username"));
        WebElement name = driver.findElement(By.id("name"));
        username.click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(CheckActiveElement_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }
        WebElement we = driver.switchTo().activeElement();
        System.out.println(we.equals(username)); // true
        System.out.println(we.equals(name));  // false
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(CheckActiveElement_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.quit();
    }
}
