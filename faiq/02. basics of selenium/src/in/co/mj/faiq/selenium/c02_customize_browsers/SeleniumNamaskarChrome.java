package in.co.mj.faiq.selenium.c02_customize_browsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumNamaskarChrome {

    public static void main(String[] args) {
        // Creating the web driver object
        WebDriver driver;

        // Instantiating driver object and launching web browser
        driver = new ChromeDriver();

        // Using get() method to open the webpage
        driver.get("https://www.amazon.in/dp/B07BQHYKM7");

        // Closing the browser
        driver.quit();
    }

}
