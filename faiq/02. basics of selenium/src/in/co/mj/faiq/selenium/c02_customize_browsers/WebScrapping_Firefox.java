package in.co.mj.faiq.selenium.c02_customize_browsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebScrapping_Firefox {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");

        WebDriver driver = new FirefoxDriver();

        driver.get("http://127.0.0.1:5000/bulma");

        String source = driver.getPageSource();
        System.out.println(source);
        driver.quit();
    }
}
