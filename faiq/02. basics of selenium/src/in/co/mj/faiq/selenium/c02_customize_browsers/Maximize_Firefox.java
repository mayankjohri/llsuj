package in.co.mj.faiq.selenium.c02_customize_browsers;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Maximize_Firefox {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        DesiredCapabilities caps = new DesiredCapabilities();
        FirefoxProfile profile = new FirefoxProfile();
        FirefoxOptions options = new FirefoxOptions();

        profile.setPreference("general.useragent.override", "God-Browser");
        options.setProfile(profile);
        caps.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
        WebDriver driver = new FirefoxDriver(caps);

        try {
            driver.get("http://127.0.0.1:5000/bulma");
            driver.manage().window().maximize();
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Maximize_Firefox.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            driver.quit();
        }
    }
}
