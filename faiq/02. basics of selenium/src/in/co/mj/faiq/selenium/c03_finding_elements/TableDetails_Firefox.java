package in.co.mj.faiq.selenium.c03_finding_elements;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TableDetails_Firefox {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://127.0.0.1:5000/table");
        
        // selecting rows
        List<WebElement> we_list = (ArrayList) driver.findElements(By.cssSelector("td.tg-us36:nth-child(2)"));
        
        for(WebElement we: we_list ){
            System.out.println(we.getText());
        }
        System.out.println("~^~^~^~^~^~^~^~^~^ cols *^**^*^*^*^*^*");
        // selecting cols
        we_list = (ArrayList) driver.findElements(By.cssSelector("tr:nth-child(2)"));
        
        for(WebElement we: we_list ){
            System.out.println(we.getText());
        }
        driver.quit();
    }
}
