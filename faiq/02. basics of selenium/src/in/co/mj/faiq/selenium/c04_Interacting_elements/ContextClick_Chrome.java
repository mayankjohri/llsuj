package in.co.mj.faiq.selenium.c04_Interacting_elements;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

public class ContextClick_Chrome {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        ChromeOptions options = new ChromeOptions();
        options.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
        ChromeDriver driver = new ChromeDriver(options);
        driver.get("http://127.0.0.1:5000/bulma");
        
        WebElement link = driver.findElement(By.cssSelector("input[id='cb_test'] ~ a"));
        
        Actions action = new Actions(driver);
        
        action.moveToElement(link).contextClick().sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.RETURN).build().perform();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ContextClick_Chrome.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        driver.quit();
    }
}
