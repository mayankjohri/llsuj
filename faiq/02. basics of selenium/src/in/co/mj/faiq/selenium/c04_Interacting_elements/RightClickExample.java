/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.mj.faiq.selenium.c04_Interacting_elements;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author mayank
 */
public class RightClickExample {
    private static WebDriver driver;

    static String URL = "https://swisnl.github.io/jQuery-contextMenu/demo.html";


    public static void main(String[] args)  {
                System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        driver = new FirefoxDriver();
        driver.get(URL);
        driver.navigate().to(URL);
        WebElement element =driver.findElement(By.cssSelector("span[class='context-menu-one btn btn-neutral']"));
        rightClick(element);
        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            Logger.getLogger(RightClickExample.class.getName()).log(Level.SEVERE, null, ex);
        }
        WebElement elementEdit =driver.findElement(By.cssSelector("[class='context-menu-item context-menu-icon context-menu-icon-edit']>span"));
        elementEdit.click();
        Alert alert=driver.switchTo().alert();
        String textEdit = alert.getText();
        System.out.println(textEdit + "clicked: edit" + "Failed to click on Edit link");
         driver.quit();
    }

    public static void rightClick(WebElement element) {
        try {
            Actions action = new Actions(driver).contextClick(element);
            action.build().perform();

            System.out.println("Sucessfully Right clicked on the element");
        } catch (StaleElementReferenceException e) {
            System.out.println(MessageFormat.format("Element is not attached to the page document {0}", 
                    Arrays.toString(e.getStackTrace())));
        } catch (NoSuchElementException e) {
            System.out.println(MessageFormat.format("Element {0} was not found in DOM {1}", element, 
                    Arrays.toString(e.getStackTrace())));
        } catch (Exception e) {
            System.out.println(MessageFormat.format("Element {0} was not clickable {1}", element,
                    Arrays.toString(e.getStackTrace())));
        }
    }

    
}
