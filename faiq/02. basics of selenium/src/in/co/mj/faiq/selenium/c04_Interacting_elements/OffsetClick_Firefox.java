package in.co.mj.faiq.selenium.c04_Interacting_elements;

import java.io.File;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class OffsetClick_Firefox {

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        File pathBinary = new File("/home/mayank/apps/firefox/firefox");
        FirefoxBinary firefoxBinary = new FirefoxBinary(pathBinary);
        DesiredCapabilities desired = DesiredCapabilities.firefox();
        FirefoxOptions options = new FirefoxOptions();
        desired.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options.setBinary(firefoxBinary));
        WebDriver driver = new FirefoxDriver(options);
        driver.get("http://127.0.0.1:5000/canvas");

        WebElement canvas = driver.findElement(By.tagName("canvas"));

        Actions action = new Actions(driver);

        action.moveToElement(canvas).moveByOffset(20, 20).moveByOffset(20, 0);
        action.moveByOffset(0, 20).moveToElement(canvas, 200, 20);
        action.perform();

        driver.quit();
    }
}
