package in.co.mj.faiq.selenium.c03_finding_elements;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.NoSuchFrameException;

public class CustomWait_Firefox {

    private static boolean waitForElementInFrame(WebDriver driver,
            final String frame_name,
            By element_by) {
        int timeOut = 90;
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                try {
                    driver.switchTo().frame("i_popups");
                    WebElement element = driver.findElement(By.id("lnkNewWindow"));
                    return Boolean.TRUE;
                } catch (NoSuchFrameException ex) {
                    System.out.println("~~~~~~~~~~~ !!! ERROR !!! ~~~~~~~~~");
//                            return Boolean.FALSE;
                    return null;
                } catch ( NoSuchElementException ex){
                    return null;
                }
            }
        });
        return true;
    }

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://127.0.0.1:5000/iframe");
        waitForElementInFrame(driver, "i_popups", By.id("lnkNewWindow"));
        WebElement element = driver.findElement(By.id("lnkNewWindow"));
        System.out.println("target: " + element.getAttribute("target"));
        driver.switchTo().defaultContent();
        try {
            Thread.sleep(2);
        } catch (InterruptedException ex) {
            Logger.getLogger(CustomWait_Firefox.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        driver.quit();
    }
}
