package in.co.mj.faiq.selenium.c02_customize_browsers;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class Hidden_Firefox_session {

    public static void main(String[] args) {

        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        FirefoxOptions options = new FirefoxOptions();
//        options.addArguments("-headless");
        options.setHeadless(true);
        options.setBinary("/home/mayank/apps/firefox/firefox"); //Location where Firefox is installed
        FirefoxDriver webdriver = new FirefoxDriver(options);
        webdriver.get("http://localhost:5000/bulma");
        String s = webdriver.findElementById("username").getAttribute("value");
        System.out.println(s);
        webdriver.quit();
    }
}
