package in.co.mj.faiq.selenium.c02_customize_browsers;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Hidden_Chrome {

    public static void main(String[] args) {

        System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);

        ChromeDriver webdriver = new ChromeDriver(options);
        webdriver.get("http://localhost:5000/bulma");
        String s = webdriver.findElementById("username").getAttribute("value");
        System.out.println(s);
        webdriver.quit();

    }
}
