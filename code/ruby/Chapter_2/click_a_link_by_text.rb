#encoding: utf-8
##############
# License: GNU 3.0
# copyright: Mayank Johri
# Note that this code will keep on updating as we progress...
# *****************  Thus this is not production ready. ********************
##############
require "selenium-webdriver"

class ClickALinkByText
  def initialize
    @verification_errors = []
    driver_path = '../../driver'
    ENV['webdriver.firefox.driver'] = driver_path
    #############################################
    # Not sure if it will work on Windows, in not then please
    # replace `:` with `;`.
    #############################################
    ENV['PATH'] = "#{driver_path}:#{ENV['PATH']}"
    @driver = Selenium::WebDriver.for :firefox

    # @driver.start
    # @driver.set_context("test_new")
  end

  def teardown
    @driver.quit
  end

  def testingConcept
    @driver.navigate.to "https://myaccount.google.com/intro?utm_source=OGB"
    myName = @driver.find_element(:link_text, "Sign in")
    puts "Page title is #{@driver.title} in testingConcept"
    myName.click()
    wait = Selenium::WebDriver::Wait.new(:timeout => 60)
    wait.until { @driver.title.downcase.start_with? "sign in" }
  end

  def getTitle
    puts("Page title is #{@driver.title} in getTitle")
end
end

# def wait_for_page_load( self, element_that_should_stale, timeout = 30 ):
#   WebDriverWait( self._b, timeout ).until(
#   	expected_conditions.staleness_of( element_that_should_stale )
#   )
# end

sc = ClickALinkByText.new
sc.testingConcept
puts("testingConcept Compeleted")
sc.getTitle
# sc.teardown
