#encoding: utf-8
##############
# License: GNU 3.0
# copyright: Mayank Johri
# Note that this code will keep on updating as we progress...
# *****************  Thus this is not production ready. ********************
##############
require "selenium-webdriver"
require "test/unit"


class SeleniumTest < Test::Unit::TestCase
  def setup
    @verification_errors = []
    driver_path = '../../driver'
    ENV['webdriver.firefox.driver'] = driver_path
    #############################################
    # Not sure if it will work on Windows, in not then please
    # replace `:` with `;`.
    #############################################
    ENV['PATH'] = "#{driver_path}:#{ENV['PATH']}"
    @selenium = Selenium::WebDriver.for :firefox
    # @selenium.start
    # @selenium.set_context("test_new")
  end

  def teardown
    @selenium.quit
  end

  def test_new
    # Replace this with your code.
    @selenium.navigate.to "https://www.google.com"

  end
end
