#encoding: utf-8
##############
# License: GNU 3.0
# copyright: Mayank Johri
# Note that this code will keep on updating as we progress...
# *****************  Thus this is not production ready. ********************
##############
require "selenium-webdriver"

class SeleniumLocators
  def initialize
    @verification_errors = []
    driver_path = '../../driver'
    ENV['webdriver.firefox.driver'] = driver_path
    #############################################
    # Not sure if it will work on Windows, in not then please
    # replace `:` with `;`.
    #############################################
    ENV['PATH'] = "#{driver_path}:#{ENV['PATH']}"
    @selenium = Selenium::WebDriver.for :firefox
    # @selenium.start
    # @selenium.set_context("test_new")
  end

  def teardown
    @selenium.quit
  end

  def testingConcept
    # Replace this with your code.
    @selenium.navigate.to "https://www.google.com"
    # searchInput = @selenium.find_element(:id, "gs_htif0")
    wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
    input = wait.until {
        element = @selenium.find_element(:id, "gs_htif0")
        element if element.displayed?
    }
    input.send_keys("Selenium Testing by Mayank")
  end
end

sc = SeleniumLocators.new
sc.testingConcept
puts("testingConcept Compeleted")
## uncomment below
# sc.teardown
