#encoding: utf-8
##############
# License: GNU 3.0
# copyright: Mayank Johri
# Note that this code will keep on updating as we progress...
# *****************  Thus this is not production ready. ********************
##############
require "selenium-webdriver"

class ClickALinkByText
  def initialize
    @verification_errors = []
    driver_path = '../../driver'
    ENV['webdriver.firefox.driver'] = driver_path
    #############################################
    # Not sure if it will work on Windows, in not then please
    # replace `:` with `;`.
    #############################################
    ENV['PATH'] = "#{driver_path}:#{ENV['PATH']}"
    @driver = Selenium::WebDriver.for :firefox

    # @driver.start
    # @driver.set_context("test_new")
  end

  def teardown
    @driver.quit
  end

  def testingConcept
    @driver.navigate.to "http://127.0.0.1:5000/chapter/3"
    myName = @driver.find_element(:css, ".links:nth-of-type(3)")
    getTitle("testingConcept")
    myName.click()
    wait = Selenium::WebDriver::Wait.new(:timeout => 60)
    wait.until { @driver.title.downcase.start_with? "hello, you clicked" }
  end

  def getTitle(location)
    puts("Page title is #{@driver.title} in " + location)
  end
end

# def wait_for_page_load( self, element_that_should_stale, timeout = 30 ):
#   WebDriverWait( self._b, timeout ).until(
#   	expected_conditions.staleness_of( element_that_should_stale )
#   )
# end

sc = ClickALinkByText.new
sc.testingConcept
puts("testing Compeleted")
sc.getTitle ("main")
# sc.teardown
