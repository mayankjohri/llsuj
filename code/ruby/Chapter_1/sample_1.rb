# #encoding: utf-8
require "selenium-webdriver"
# #Firefox browser instantiation

# Selenium 3 uses Marionette by default when firefox is specified
# Set Marionette in Selenium 2 by directly passing marionette: true
# You might need to specify an alternate path for the desired version of Firefox

# Selenium::WebDriver::Firefox::Binary.path = "/usr/bin/firefox"

# # On Mac OS you must point to the binary executable
# # Selenium::WebDriver::Firefox::Binary.path = "/Applications/Firefox.app/Contents/MacOS/firefox"

# driver = Selenium::WebDriver.for :firefox, marionette: true

##########################
# HERE DOCUMENTS                        #
##########################
# Here document is a construction known from the Unix Shell, 
# allows to build a string from the multiple lines. Like in the 
# Shell, you must specify a symbol used to identify the end of
# the string preceded by <<. Notice there must be no space
#between << and the identifier
#########################

# Setting the path gecho driver
driver_path = '../../driver'
ENV['webdriver.gecko.driver'] = driver_path + "gechodriver"
puts ENV['webdriver.firefox.driver']
ENV['PATH'] = "#{driver_path}:#{ENV['PATH']}"




driver = Selenium::WebDriver.for :firefox, marionette: true
driver.navigate.to "http://google.com"

element = driver.find_element(:name, 'q')
element.send_keys "Selenium Tutorials by Mayank Johri"
element.submit

driver.quit
