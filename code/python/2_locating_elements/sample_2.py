from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import sys
import os


caps = DesiredCapabilities.FIREFOX

# Tell the Python bindings to use Marionette.
# This will not be necessary in the future,
# when Selenium will auto-detect what remote end
# it is talking to.
caps["marionette"] = True

# Path to Firefox DevEdition or Nightly.
# Firefox >= 50.
#
# On Mac OS you must point to the binary executable
# inside the application package, such as
# /Applications/FirefoxNightly.app/Contents/MacOS/firefox-bin
path = os.environ['PATH']
caps["binary"] = "/usr/bin/firefox"
path = sys.path.append("../../driver")
print(path)
print("-" * 80)
print(path)
driver = webdriver.Firefox(capabilities=caps)
driver.get("http://google.com")
driver
driver.quit()
