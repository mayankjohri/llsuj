from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

def get_custom_firefox(path):
    # Do not use the relative path :(
    # binary = FirefoxBinary('~/apps/firefox/firefox')
    binary = FirefoxBinary(path)
    return webdriver.Firefox(firefox_binary=binary)

