Python 3 Tutorial
http://www.python-course.eu/python3_course.php

The Origins of Python
Starting with Python: The Interactive Shell
Executing a Script
Indentation
Data Types and Variables
Operators
Sequential Data Types: Lists and Strings
List Manipulations
Shallow and Deep Copy
Dictionaries
Sets and Frozen Sets
input via the keyboard
Conditional Statements
Loops, while Loop
For Loops
Output with Print
Formatted output with string modulo and the format method
Functions
Recursion and Recursive Functions
Parameter Passing in Functions
Namespaces
Global and Local Variables
Decorators
Memoization with Decorators
Read and Write Files
Modular Programming and Modules
Regular Expressions
Regular Expressions, Advanced
Lambda Operator, Filter, Reduce and Map
List Comprehension
Iterators and Generators
Exception Handling
Tests, DocTests, UnitTests
Object Oriented Programming
Class and Instance Attributes
Properties vs. getters and setters
Inheritance
Multiple Inheritance
Magic Methods and Operator Overloading
OOP, Inheritance Example
Slots
Classes and Class Creation
Road to Metaclasses
Metaclasses
Metaclass Use Case: Count Function Calls
