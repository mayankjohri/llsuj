from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

# Do not use the relative path :( 
# binary = FirefoxBinary('~/apps/firefox/firefox')
binary = FirefoxBinary('/home/mayank/apps/firefox/firefox')
browser = webdriver.Firefox(firefox_binary=binary)
browser.get('http://seleniumhq.org/')
print(browser.title)

