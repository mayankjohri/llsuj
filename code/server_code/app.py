"""Web Site for Web Testing.

This site can be used for learning Selenium.
"""

from flask import Flask, render_template
app = Flask(__name__)


@app.route('/')
def index():
    """."""
    return render_template('index.html')


@app.route('/generic')
def generic():
    """."""
    return render_template('generic.html')


@app.route('/elements')
def elements():
    """."""
    return render_template('elements.html')


@app.route('/link/<name>')
def clicked_Link(name):
    """."""
    return """<title>Hello, You clicked {0}th link!</title><html>
                      Hello, You clicked {0}th link!,
                      <a href='javascript:window.history.back();'>
                      Click Me</a></html>""".format(name)


@app.route('/chapter/<chapter_number>')
def chapters(chapter_number):
    """."""
    chapter_url = 'chapters/chapter_' + str(chapter_number) + '.html'
    return render_template(chapter_url)


if __name__ == "__main__":
    app.run(debug=True)
