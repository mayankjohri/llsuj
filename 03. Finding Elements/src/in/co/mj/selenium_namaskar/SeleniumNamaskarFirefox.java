package in.co.mj.selenium_namaskar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumNamaskarFirefox {

	public static void main(String[] args) {
		// Creating the web driver object
		WebDriver driver;

		// Instantiating driver object and launching web browser
		System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
		driver = new FirefoxDriver();

		// Using get() method to open the webpage
		driver.get("https://www.amazon.in/dp/B07BQHYKM7");

		// Closing the browser
		driver.quit();
	}

}
