/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.in.mj.cbt.cbtexample.framework.utils;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author mayank
 */
public class MayaUtils {

    public WebDriver select_browser_v1(String browser_name) {
        return select_browser_v1(browser_name, false);
    }

    /**
     *
     * @param browser_name
     * @param proxy
     * @return
     */
    public WebDriver select_browser_v1(String browser_name, boolean proxy) {
        WebDriver wd = null;
        DesiredCapabilities cap = new DesiredCapabilities();

        if (proxy == true) {
            Proxy p = new Proxy();
            // please update the /etc/hosts or 
            // %windir%/system32/drivers/etc/hosts)file with 
            // <your_proxy_ip>  proxy
            p.setHttpProxy("proxy:8080");
            cap.setCapability(CapabilityType.PROXY, p);
        }
        switch (browser_name) {
            case "chrome":
                wd = new ChromeDriver(cap);
                break;
            case "firefox":
                System.setProperty("webdriver.gecko.driver",
                        "/home/mayank/apps/web_drivers/geckodriver");
                wd = new FirefoxDriver(cap);
                break;
            case "opera":
                System.setProperty("webdriver.opera.driver",
                        "/home/mayank/apps/web_drivers/operadriver");

                wd = new OperaDriver();
                break;
        }
        return wd;
    }
}
