/*
 * Copyright (C) 2018 mayank
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package main.in.mj.art.framework.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mayank
 */
public class MultiLang {

    Connection conn = null;
    String url;

    public MultiLang() {
        //String fileName
        url = "jdbc:sqlite:"
                + getClass().getResource("/resources/lang/text.sqlite3");
    }

    private Connection connect() {
        // SQLite connection string

        conn = null;
        System.out.println(url);
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(MessageFormat.format("Error Message{0}", e.getMessage()));
        }
        return conn;
    }

    public String getData(String key, String lang) {
        String qry = "select val from data where key=\"{0}\" and lang={1}";
        String sql = MessageFormat.format(qry, key, lang);
        try {
            conn = connect();

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            String val = null;
            while (rs.next()) {
                val = rs.getString("val");
                disconnect(conn);
                conn = null;
                break;
            }
            return val;
        } catch (SQLException e) {
            System.out.println(MessageFormat.format("Error Message{0}", e.getMessage()));
        }

        return "";
    }

    private void disconnect(Connection conn) {
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MultiLang.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
