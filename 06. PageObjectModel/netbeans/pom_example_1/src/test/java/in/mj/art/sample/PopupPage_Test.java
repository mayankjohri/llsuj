/*
 * Copyright (C) 2018 mayank
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package test.java.in.mj.art.sample;

import main.in.mj.app.pageobjects.PopupsPage;
import main.in.mj.art.framework.utils.MayaUtils;
import main.in.mj.art.framework.utils.MultiLang;
import org.openqa.selenium.WebDriver;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 *
 * @author mayank
 */
public class PopupPage_Test {

    private WebDriver wd;
    private MayaUtils mu;
    private PopupsPage pp;
    private MultiLang ml;

    public PopupPage_Test() {
        ml = new MultiLang();
    }

 
    @Test
    public void show_confirm_box_button() {
        assertEquals(pp.clickConfirm_show_confirm_box_button(),
                "Choose an option.");
    }

    @Test
    public void show_confirm_box_button_click() {
        pp.clickConfirm_show_confirm_box_button();
        assertEquals(pp.getOutput(), "Confirmed.");
    }

    @Test
    public void validate_ref_text(){
        String txt = pp.getreference_text();
        String expected = ml.getData("popups_ref", "1");
        assertEquals(txt, expected);
        
    }
    @BeforeClass
    @Parameters("browser_type")
    public void setUpClass(String browserName) throws Exception {
        mu = new MayaUtils();
        wd = mu.select_browser_v1(browserName);
        pp = new PopupsPage(wd);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
        wd.close();
        wd.quit();
        mu = null;
        pp = null;
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
        System.out.println("URL: ".concat(wd.getCurrentUrl()));
        Thread.sleep(2);
        pp.goPage();
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
}
