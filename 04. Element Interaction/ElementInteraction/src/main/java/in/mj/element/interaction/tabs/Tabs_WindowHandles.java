/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.tabs;

/**
 *
 * @author mayank
 */
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Tabs_WindowHandles {

    public static void main(String[] args) throws InterruptedException {
        String URL = "http://localhost:5000/popups";
        String bulma_url = "http://localhost:5000/bulma";        
        boolean px = false;
        DesiredCapabilities cap = new DesiredCapabilities();

        if (px == true) {
            Proxy p = new Proxy();
            p.setHttpProxy("proxy:8080");
            cap.setCapability(CapabilityType.PROXY, p);
        }
        WebDriver driver = new ChromeDriver(cap);
        driver.manage().window().maximize();
        driver.get(URL);
        String parentWindow = driver.getWindowHandle();
        System.out.println("Title (Parent Window): " + driver.getTitle());
        driver.findElement(By.id("lnkNewWindow")).click();
        //Get All Tabs or Window handles and 
        // iterate till desired one is found.
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);
            System.out.println(handle);
            String new_url = driver.getCurrentUrl();
            System.out.println(new_url);
            if (new_url.equals(bulma_url)) {
                break;
            }
        }

        //Enter the text "Mayank Johri" in username field
        WebElement we = driver.findElement(By.cssSelector("body > div > div:nth-child(1)"
                + " > div > input"));
        we.sendKeys("Mayank Johri");
        //Lets move back to parent window 
        driver.switchTo().window(parentWindow);
        Thread.sleep(3);
    }
}
