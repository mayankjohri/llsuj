/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.iframes_frames;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 *
 * @author mayank
 */
public class FrameExample {

    private static final Logger LOG = Logger.getLogger(
            Logger.GLOBAL_LOGGER_NAME);

    public void getData() throws InterruptedException {
        WebDriver wd = null;
        try {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.setBinary("/home/mayank/apps/chrome/chrome");
            wd = new ChromeDriver(chromeOptions);
            wd.navigate().to("http://localhost:5000/frame");
            WebElement we = wd.findElement(By.id("menu"));
            String parent = wd.getWindowHandle();
            wd.switchTo().frame(we);
            String css = "#l1";
            LOG.setLevel(Level.INFO);
            WebElement ele = wd.findElement(By.cssSelector(css));
            ele.click();
            LOG.log(Level.INFO, "Element Text: {0}", ele.getText());
        } catch (Exception ex) {
            LOG.log(Level.INFO, "Error: {0}", ex.getMessage());
        } finally {
            if (wd != null) {
                wd.switchTo().defaultContent();
                Thread.sleep(3000);
                wd.close();
                wd.quit();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        FrameExample egt = new FrameExample();
        egt.getData();
    }
}
