/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction;

/**
 *
 * @author mayank
 */
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ElementClear_Fixed {

    private static final Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public void getData() {
        WebDriver webdriver = null;
        try {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.setBinary("/home/mayank/apps/chrome/chrome");
            webdriver = new ChromeDriver(chromeOptions);
            webdriver.navigate().to("http://127.0.0.1:5000/bulma");
            WebElement bot;
            ArrayList<String> al = new ArrayList<>();
            al.add("[placeholder='Email input']");
            al.add("[placeholder='Textarea']");

            LOG.setLevel(Level.INFO);
            for (String css : al) {
                bot = webdriver.findElement(By.cssSelector(css));

                LOG.log(Level.INFO, "label value before: {0}", bot.getAttribute("value"));
                bot.clear();
                LOG.log(Level.INFO, "label value after: {0}", bot.getAttribute("value"));
            }

        } catch (Exception ex) {
            LOG.log(Level.INFO, "label value: {0}", ex.getMessage());
        } finally {
            if (webdriver != null) {
                webdriver.close();
                webdriver.quit();
            }
        }
    }

    public static void main(String[] args) {
        ElementClear_Fixed egt = new ElementClear_Fixed();
        egt.getData();
    }
}
