/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.actionchains;

/**
 *
 * @author mayank
 */
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

public class ClickAndHoldMoveAndRelease {

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = null;
        String URL = "http://localhost:5000/drag_and_drop";
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setBinary("/home/mayank/apps/chrome/chrome");
        driver = new ChromeDriver(chromeOptions);
        driver.get(URL);

//        It is always advisable to Maximize the window 
//        before performing DragNDrop action
        driver.manage().window().maximize();
        WebElement Sourcelocator = driver.findElement(
                By.className("ui-draggable"));
        WebElement Destinationlocator = driver.findElement(
                By.className("ui-droppable"));
//        (new Actions(driver)).dragAndDrop(Sourcelocator, 
//                Destinationlocator).perform();
//      we are trying to do what above code can do in a line :)
        (new Actions(driver)).clickAndHold(Sourcelocator)
                .moveToElement(Destinationlocator).release()
                .build().perform();
//        //Thread.sleep just for user to notice the event
        Thread.sleep(3000);
//        //Closing the driver instance
        driver.quit();
    }

}
