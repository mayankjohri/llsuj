/*
 * Copyright (C) 2018 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package in.mj.element.interaction.windows;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import in.mj.element.interaction.utils.Mayautils;

/**
 *
 * @author mayank
 */
public class WindowsExamples_firefox_1 {

    private static final Logger LOG = Logger.getLogger(
            Logger.GLOBAL_LOGGER_NAME);
    private static String mainwindow ;
    public void getData() throws InterruptedException {
        WebDriver wd = null;
        try {
            LOG.setLevel(Level.INFO);
            Mayautils mu = new Mayautils();
            wd = mu.select_browser_v1("ff");
            wd.navigate().to("http://127.0.0.1:5000/links");
            WebElement newPopup = wd.findElement(By.id("newPopup"));
            newPopup.click();
            mainwindow = wd.getWindowHandle();
            for (String windowId : wd.getWindowHandles()) {
                String title = wd.switchTo().window(windowId).getTitle();
                if (title.equals("Welcome Success")) {
                    LOG.log(Level.INFO, "Title: {0}", wd.getTitle());
                    break;
                }
            }
            // Lets work on popup Window
            String css = "#success";
            WebElement ele = wd.findElement(By.cssSelector(css));
            LOG.log(Level.INFO, "Element Text: {0}", ele.getText());

            wd.switchTo().window(mainwindow);
            wd.findElement(By.id("menu")).click();
            for (String windowId : wd.getWindowHandles()) {
                String title = wd.switchTo().window(windowId).getTitle();
                if (title.contains("bootstrap menu example")) {
                    LOG.log(Level.INFO, "Title: {0}", wd.getTitle());
                    break;
                }
            }        
        } catch (Exception ex) {
            LOG.log(Level.INFO, "Error: {0}", ex.getMessage());
        } finally {
            if (wd != null) {
                wd.switchTo().window(mainwindow);
                Thread.sleep(3000);
                wd.close();
                wd.quit();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        WindowsExamples_firefox_1 egt = new WindowsExamples_firefox_1();
        egt.getData();
    }
}
