package in.mj.ElementProperties;

/**
 *
 * @author mayank
 */
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class GetElementLocation {

    private static final Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public void getData() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setBinary("/home/mayank/apps/chrome/chrome");
        chromeOptions.addArguments("--headless");
        WebDriver webdriver = new ChromeDriver(chromeOptions);
        webdriver.navigate().to("http://127.0.0.1:5000/");
        WebElement ele = webdriver.findElement(By.cssSelector("input[name='vehicle'][value='Bike']"));
        LOG.setLevel(Level.INFO);
        LOG.log(Level.INFO, "Vals: {0}", ele.getLocation());
        webdriver.close();
        webdriver.quit();
    }

    public static void main(String[] args) {
        GetElementLocation egt = new GetElementLocation();
        egt.getData();
    }
}
