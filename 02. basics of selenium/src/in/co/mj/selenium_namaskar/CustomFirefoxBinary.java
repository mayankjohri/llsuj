package in.co.mj.selenium_namaskar;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class CustomFirefoxBinary {

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver", "/home/mayank/apps/web_drivers/geckodriver");
		FirefoxOptions options = new FirefoxOptions();
		options.setBinary("/home/mayank/apps/firefox/firefox"); //Location where Firefox is installed
		FirefoxOptions firefoxOptions = new FirefoxOptions();
	    firefoxOptions.setCapability("moz:firefoxOptions", options);
	    FirefoxDriver webdriver = new FirefoxDriver(firefoxOptions);
	    webdriver.get("https://www.amazon.in/dp/B07BQHYKM7");
	}
	

}

